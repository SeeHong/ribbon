import React, { Component } from 'react';
import { SafeAreaView, StatusBar } from "react-native";
import Navigator from './src/router';
import { connect } from 'react-redux';
import NavigationService from './NavigationService';
import {setLanguage} from './src/translation/i18n';
import { getColor } from './src/colors';
// import firebase from 'react-native-firebase';

class App extends Component {

  constructor(props) {
    super(props);
    if(props.App.Auth.locale){
      this.onLanguageSet(props.App.Auth.locale);
    }
    StatusBar.setBackgroundColor(getColor("APP_1"));
    StatusBar.setBarStyle('dark-content');
  }

  onLanguageSet(lang){
    setLanguage(lang);
  }

  // connectFirebase() {
  //   const channel = new firebase.notifications.Android.Channel(
  //     "lapar_order_channel",
  //     'Order Channel',
  //     firebase.notifications.Android.Importance.Max
  //   ).setDescription('An Android channel for this session being used by local push notifications.');

  //   firebase.notifications().android.createChannel(channel);
  // }
  
  render(){
   
    return (
    <SafeAreaView style={{flex: 1}}>
      <Navigator 
      ref={navigatorRef => {
        NavigationService.setTopLevelNavigator(navigatorRef);
      }}
      screenProps={{reset: ()=>this.setState({})}}
      />
    </SafeAreaView>)
  }
}

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
