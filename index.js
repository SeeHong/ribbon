import {AppRegistry, View, Image, Dimensions} from 'react-native';
import App from './App';
import React from 'react';
import {name as appName} from './app.json';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import store from './src/redux/store';
import { getColor } from './src/colors';

const RNRedux = () => (
    <Provider store = { store.store }>
      <PersistGate loading={<View style={{alignItems:"center", justifyContent:'center', flex:1, backgroundColor: getColor('GREY_LIGHTEN_4')}}>
        <Image
        source={require('./src/assets/logo_main.png')}
        style={{width: Dimensions.get('window').width/2, height: Dimensions.get('window').width/2}}
        resizeMode="contain"
        />
      </View>} 
          persistor={store.persistor}>
        <App />
      </PersistGate>
    </Provider>
)

AppRegistry.registerComponent(appName, () => RNRedux);
