import TimerMixin from 'react-timer-mixin';
import { ToastAndroid,Platform } from 'react-native';
import {APP_API_URL, APP_DEBUG, APP_FILE_URL} from './config';
import {Alert} from 'react-native';
import Store from './redux/store';
import {reset} from './redux/actions/AuthActions';
import NavigationService from '../NavigationService';

export const ajax = (obj)=>{
    const method = obj.method ? obj.method : "POST";
    const header = {'Accept': 'application/json', 'Content-Type': 'application/json', ...Store.store.getState().Auth.cookies};
    const body = JSON.stringify(obj.data);
    if(APP_DEBUG){
        console.log(APP_API_URL + obj.url, body);
    }
    return Promise.race([
        new Promise((resolve, reject) =>
            fetch(APP_API_URL + obj.url, {
                method: method,
                headers: header,
                body: body,
                credentials: 'include',
                withCredentials: true,
                timeout: 2000
            })
            .then((response) => {
                response.json().then(response_json=>{
                    if(response.status==200){
                        resolve(response_json);
                    }else{
                        reject(response_json);
                    }
                })
            })
            .catch((error) => {
                if(APP_DEBUG){
                    console.log(error.toString());
                }
                ToastAndroid.show(error.toString(), ToastAndroid.SHORT);
                reject(error);
            })
        ),
        timeout(15000)
    ]).then(responseJson=>{
        if(APP_DEBUG){
            console.log(APP_API_URL + obj.url, responseJson);
        }
        obj.complete?obj.complete():null;
        if(responseJson == "ConnectionTimeout"){
            obj.fail?obj.fail({message: "Connection Timeout"}):null;
            ToastAndroid.show('Connection Timeout', ToastAndroid.SHORT);
            return false;
        }
        if(!responseJson.error){
            obj.success?obj.success(responseJson):null;
        }else{
            obj.fail?obj.fail(responseJson):null;
        }
    }).catch(e=>{
        if(APP_DEBUG){
            console.log(APP_API_URL + obj.url, e);
        }
        obj.complete?obj.complete():null;
        obj.fail?obj.fail(e):null;
        obj.isShowAlert?Alert.alert('', e.message):null;
    })
}

export const fileUploadAjax = (obj)=>{
    const method = "POST";
    const header = {'Accept': 'application/json', 'Content-Type': 'multipart/form-data', 'access-token': Store.store.getState().Auth.api_token};
    const body = new FormData();
    body.append("image", {
        name: obj.data.image.fileName,
        type: obj.data.image.type,
        uri: Platform.OS === "android" ? obj.data.image.uri : obj.data.image.uri.replace("file://", "")
    });

    Object.keys(obj.data).forEach(key => {
        if(key!="image"){
            body.append(key, obj.data[key]);
        }
    });
    if(APP_DEBUG){
        console.log(APP_API_URL + obj.url, body);
    }
    return Promise.race([
        new Promise((resolve, reject) =>
            fetch(APP_API_URL + obj.url, {
                method: method,
                headers: header,
                body: body
            })
            .then((response) => response.json())
            .then((responseJson) => {
                if(APP_DEBUG){
                    console.log(APP_API_URL + obj.url, responseJson);
                }
                if(responseJson.isSuccess){
                    obj.success? obj.success(responseJson):null;
                }else{
                    obj.fail?obj.fail(responseJson):null;
                    obj.isShowAlert?Alert.alert('', responseJson.message): null;
                    if(responseJson.code=="AUTH_REQUIRED"){
                        Store.store.dispatch(Reset());
                        NavigationService.navigate('Login');
                    }
                }
                obj.complete?obj.complete():null;
                resolve(responseJson);
            })
            .catch((error) => {
                if(APP_DEBUG){
                    console.log(error);
                }
                Alert.alert('', "Something went wrong. Please Contact Administrator")
                reject('Something went wrong. Please Contact Administrator');
            })
        ),
        timeout(10000)
    ])
}

export const timeout = (ms = 10000) =>
{
    return new Promise((resolve, reject) =>TimerMixin.setTimeout(() => resolve('ConnectionTimeout'), ms));
}