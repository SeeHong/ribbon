export const APP_DEBUG=true;
export const APP_API_URL = "https://devapis.ribbon.my/api/v1/";
export const APP_FILE_URL = "https://devapis.ribbon.my/";
// export const APP_API_URL = "http://192.168.0.154/LaparAPI/public/api/";
// export const APP_FILE_URL = "http://192.168.0.154/LaparAPI/public/";
// export const APP_API_URL = "http://lapar.gonow.my/api/";
// export const APP_FILE_URL = "http://lapar.gonow.my/";

export const APP_VERSION = 1;
export const APP_VERSION_NAME="1.0.0";

export const PRIVACY_POLICY_URL_EN = "https://confession.hanotech.online/privacy_policy";
export const ABOUT_US_URL_EN = "https://confession.hanotech.online/about_us";
export const HELP_URL = "https://confession.hanotech.online/recall_submission";
export const TERMS_AND_CONDITIONS_URL_EN = "https://confession.hanotech.online/terms_and_conditions";
export const APPLE_STORE_URL="https://play.google.com/store/apps/details?id=com.whatsapp.w4b";
export const GOOGLE_STORE_URL="https://play.google.com/store/apps/details?id=com.whatsapp.w4b";