import Store from './redux/store';

export const getNickname=user=>{
    let name = user.name;
    Store.store.getState().Contacts.contacts.forEach(element => {
        if(element.get_user.id==user.id){
            name=element.nickname;
        }
    });
    return name;
}

export const parsePrice=x=>{
    return parseFloat(x).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}   

export const checkContact=user=>{
    let exists = false;
    Store.store.getState().Contacts.contacts.forEach(element => {
        if(element.get_user.id==user.id){
            exists=true;
        }
    });
    return exists;
}

// export const getNicknameByUserId=user=>{
//     let name = user.name;
//     Store.store.getState().Contacts.contacts.forEach(element => {
//         if(element.get_user.id==user.id){
//             name=element.nickname;
//         }
//     });
//     return name;
// }