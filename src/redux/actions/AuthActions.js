export const AuthActions = {
    SET_IDENTITY : "SET_IDENTITY",
    SET_LOCALE: "SET_LOCALE",
    SET_FIRST_TIME: "SET_FIRST_TIME",
    RESET: "RESET",
    SET_COOKIES: "SET_COOKIES"
}

export const setIdentity = data => async dispatch=>{
    dispatch({
        type: AuthActions.SET_IDENTITY,
        payload: data
    })
}

export const setCookies=data => async dispatch=>{
    dispatch({
        type: AuthActions.SET_COOKIES,
        payload: data
    })
}

export const setLocale = data => async dispatch=>{
    dispatch({
        type: AuthActions.SET_LOCALE,
        payload: data
    })
}

export const reset = data => async dispatch=>{
    dispatch({
        type: AuthActions.RESET,
        payload: data
    })
}