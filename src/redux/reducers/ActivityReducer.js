const initialState = {
    activities: [
        {
          img: "https://images.unsplash.com/photo-1476480862126-209bfaa8edc8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80",
          text: "Sport",
          id: 1
        },
        {
          img: "https://images.unsplash.com/photo-1573855619003-97b4799dcd8b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80",
          text: "Shopping",
          id: 2
        },
        {
          img: "https://images.unsplash.com/photo-1553383398-c40c57291c60?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=954&q=80",
          text: "Sight Seeing",
          id: 3
        },
        {
          img: "https://images.unsplash.com/photo-1582731595896-f959c4fb1509?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=978&q=80",
          text: "Game",
          id: 4
        },
        {
          img: "https://images.unsplash.com/photo-1527529482837-4698179dc6ce?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80",
          text: "Event",
          id: 5
        },
        {
          img: "https://images.unsplash.com/photo-1478720568477-152d9b164e26?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80",
          text: "Movie",
          id: 6
        },
      ],
};
import {ActivityActions} from '../actions/ActivityActions';
  
const ActivityReducer = (state = initialState, action) => {
    switch(action.type) {
        
        default:
            return state;
    }
  }

export default ActivityReducer;