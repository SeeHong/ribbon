const initialState = {
    locale: "en",
    first_time: true,
    contact: "",
    name: "",
    email: "",
    gender: "",
    birthDate: "",
    biography: "",
    voiceIntroduction: "",
    id: 0,
    cookies: null
};
import {AuthActions} from '../actions/AuthActions';
  
const AuthReducer = (state = initialState, action) => {
    switch(action.type) {
        case AuthActions.SET_IDENTITY: {
            let data = {
                id: action.payload.id,
                contact: action.payload.contact,
                name: action.payload.name,
                email: action.payload.email,
                gender: action.payload.gender,
                birthDate: action.payload.birthDate,
                biography: action.payload.biography,
                voiceIntroduction: action.payload.voiceIntroduction
            }
            return {...state, ...data}
        }
        case AuthActions.SET_COOKIES: {
            return {...state, cookies: action.payload}
        }
        case AuthActions.RESET: {
            return {...initialState, address: state.address};
        }
        default:
            return state;
    }
  }

export default AuthReducer;