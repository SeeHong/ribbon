import React from 'react';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import reduxThunk  from 'redux-thunk';
import storage from '@react-native-community/async-storage';
import AuthReducer from './reducers/AuthReducer';
import ActivityReducer from './reducers/ActivityReducer';

const persistConfig = {
  key: 'root',
  storage: storage,
  blacklist: []
}

const rootReducer = combineReducers({
    Auth : AuthReducer,
    Activity: ActivityReducer
});

const persistedReducer = persistReducer(persistConfig, rootReducer)

const configureStore = () => {
  const store = createStore(persistedReducer, applyMiddleware(reduxThunk));
  const persistor = persistStore(store);
  return {store, persistor}
}

export default configureStore();