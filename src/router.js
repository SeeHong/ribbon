import React  from 'react';
import {Text, Image} from 'react-native';
import {Icon} from 'react-native-elements';
import {strings} from './translation/i18n';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import {createStackNavigator} from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import {getColor} from './colors';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { Transition } from 'react-native-reanimated';
import Login from './screens/Login';
import VersionOutdated from './screens/VersionOutdated';
import LoginAuth from './screens/LoginAuth';
import Setup from './screens/Setup';
import Settings from './screens/Settings';
import RibbonProfile from './screens/RibbonProfile';
import EditProfile from './screens/EditProfile';
import RibbonPreferences from './screens/RibbonPreferences';
import Search from './screens/Search';
import ChatRoom from './screens/ChatRoom';
import SelectDate from './screens/SelectDate';

//app
import Home from './screens/Home';
import Saved from './screens/Saved';
import Chat from './screens/Chat';
import Notification from './screens/Notification';
import Profile from './screens/Profile';

const HomeTab = createStackNavigator({
	Home: { screen: Home, navigationOptions: { header: null}},
	RibbonProfile: {screen: RibbonProfile, navigationOptions: { header: null}},
	Search: {screen: Search, navigationOptions: { header: null}},
	SelectDate: {screen: SelectDate, navigationOptions: { header: null}}
},{
	initialRouteName: "Home",
});

HomeTab.navigationOptions = ({ navigation }) => {
	let tabBarVisible;
	if (navigation.state.routes.length > 1) {
		tabBarVisible = false;
	}
	return {
	  tabBarVisible
	};
}

const SavedTab = createStackNavigator({
	Saved: { screen: Saved, navigationOptions: { header: null}},
	RibbonProfile: {screen: RibbonProfile, navigationOptions: { header: null}}
},{
	initialRouteName: "Saved",
});

SavedTab.navigationOptions = ({ navigation }) => {
	let tabBarVisible;
	if (navigation.state.routes.length > 1) {
		tabBarVisible = false;
	}
	return {
	  tabBarVisible
	};
}

const ChatTab = createStackNavigator({
	Chat: { screen: Chat, navigationOptions: { header: null}},
	ChatRoom: {screen: ChatRoom, navigationOptions: { header: null}},
},{
	initialRouteName: "Chat",
});

ChatTab.navigationOptions = ({ navigation }) => {
	let tabBarVisible;
	if (navigation.state.routes.length > 1) {
		tabBarVisible = false;
	}
	return {
	  tabBarVisible
	};
}

const ProfileTab = createStackNavigator({
	Profile: { screen: Profile, navigationOptions: { header: null}},
	Settings: {screen: Settings, navigationOptions: { header: null}},
	EditProfile: {screen: EditProfile, navigationOptions: { header: null}},
	RibbonPreferences: {screen: RibbonPreferences, navigationOptions: { header: null}},
},{
	initialRouteName: "Profile",
});

ProfileTab.navigationOptions = ({ navigation }) => {
	let tabBarVisible;
	if (navigation.state.routes.length > 1) {
		tabBarVisible = false;
	}
	return {
	  tabBarVisible
	};
}


const App = createBottomTabNavigator(
	{
		Home: { screen: HomeTab },
		Saved: { screen: SavedTab },
		Chat: { screen: ChatTab },
		Notification: { screen: Notification },
		Profile: { screen: ProfileTab },
	},
	{
	  initialRouteName: "Home",
	  defaultNavigationOptions: ({ navigation }) => ({
		tabBarIcon: ({ focused, horizontal, tintColor }) => {
		  const { routeName } = navigation.state;
		  const focus_color = focused ? getColor('APP_1'): "#bdbdbd";
		  let icon_name = "";
		  let icon_type = "";
		  switch(routeName){
			case "Home" : {
				return (<Image
					source={focused?require('./assets/logo_active.png'):require('./assets/logo_inactive.png')}
					resizeMode="contain"
					style={{height: 30, width: 30}}
				/>)
			}
			case "Saved" : {
			  icon_name = "heart";
			  icon_type="antdesign";
			  break;
			}
			case "Chat" : {
				icon_name = "chat";
				icon_type="material";
				break;
			}
			case "Notification" : {
			  icon_name = "bell";
			  icon_type="feather";
			  break;
			}
			case "Profile" : {
				icon_name = "user";
				icon_type="font-awesome";
				break;
			  }
		  }
		  return (<Icon
			name={icon_name}
			type={icon_type}
			color={focus_color}
		  />)
		},
		tabBarLabel: ({focused})=>{
			const { routeName } = navigation.state;
			const focus_color = focused ? getColor('APP_1'): "#bdbdbd";
			switch(routeName){
			  case "Home" : {
				return <Text style={[{textAlign:"center", 
				fontSize: 11, lineHeight: 12,
				color: focus_color}]}>{strings('ribbon')}</Text>
			  }
			  case "Saved" : {
				return <Text style={[{textAlign:"center", 
				fontSize: 11, lineHeight: 12,
				color: focus_color}]}>{strings('saved')}</Text>
			  }
			  case "Chat" : {
				return <Text style={[{textAlign:"center", 
				fontSize: 11, lineHeight: 12,
				color: focus_color}]}>{strings('chat')}</Text>
			  }
			  case "Notification" : {
				return <Text style={[{textAlign:"center", 
				fontSize: 11, lineHeight: 12,
				color: focus_color}]}>{strings('notification')}</Text>
			  }
			  case "Profile" : {
				return <Text style={[{textAlign:"center", 
				fontSize: 11, lineHeight: 12,
				color: focus_color}]}>{strings('profile')}</Text>
			  }
			}
		}
	  })
	}
);

const SwitchNavigator = createAnimatedSwitchNavigator(
    {
		Login: Login,
		VersionOutdated: VersionOutdated,
		Register: LoginAuth,
		App: App,
		Setup: Setup
    },
    {
        headerMode: 'none',
		transition: (
			<Transition.Together>
			<Transition.Out
				type="slide-bottom"
				durationMs={400}
				interpolation="easeIn"
			/>
			<Transition.In type="fade" durationMs={500} />
			</Transition.Together>
		),
		initialRouteName: "Login"
    }
);

export default Route = createAppContainer(SwitchNavigator);