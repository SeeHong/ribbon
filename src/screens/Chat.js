import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, FlatList} from "react-native";
import {container} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import {text} from '../styles';
import {chat} from '../template';
import ChatRoom from './components/ChatRoom';

class Chat extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount(){
  }


  render() {
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_5')}]}>
      <View style={{padding: '10%', borderBottomWidth: 0.5, borderColor: getColor('GREY'), marginBottom: 15}}>
        <Text style={[text.title, {fontSize: 28}]}>{strings('chat')}</Text>
      </View>
      <FlatList
      data={chat}
      renderItem={({item, index})=>(<ChatRoom chat={item} index={index} onPress={()=>this.props.navigation.navigate('ChatRoom', {chat: item})}/>)}
      style={{paddingHorizontal: '5%'}}
      keyExtractor={item => item.id.toString()}
      />
    </View>)
  }
}

const styles = StyleSheet.create({

})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)