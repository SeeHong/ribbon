import React, { Component } from 'react';
import { View, Text, Image, StyleSheet} from "react-native";
import {container} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import HeaderColor from './components/HeaderColor';
import { GiftedChat } from 'react-native-gifted-chat'
import LinearGradient from 'react-native-linear-gradient';

class ChatRoom extends Component {

  constructor(props) {
    super(props);
    this.state = {
        title: this.props.navigation.getParam('chat').name,
        messages: [
            {
                _id: 1,
                text: 'Hi there! I\'m '+this.props.navigation.getParam('chat').name,
                createdAt: new Date(),
                user: {
                    _id: 2,
                    name: this.props.navigation.getParam('chat').name,
                    avatar: this.props.navigation.getParam('chat').img
                }
            },
            {
                _id: 2,
                text: 'What is your name?',
                createdAt: new Date(),
                user: {
                    _id: 2,
                    name: this.props.navigation.getParam('chat').name,
                    avatar: this.props.navigation.getParam('chat').img,
                }
            }
        ]
    }
    this.onSend=this.onSend.bind(this);
  }

  componentDidMount(){
  }

  onSend(message){
    this.setState({messages: GiftedChat.append(this.state.messages, message)})
  }

  render() {
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_4')}]}>
        <HeaderColor title={this.state.title} renderBackButton={true} navigation={this.props.navigation} 
        renderRightAction={true} rightButtonName="dots-three-vertical" rightButtonType="entypo" rightButtonAction={this.updateProfile}/>
        <LinearGradient style={{flex: 1}} colors={[getColor('APP_1'), getColor('APP_2')]}>
            <GiftedChat
            messages={this.state.messages}
            onSend={this.onSend}
            user={{
                _id: 1,
            }}
            />
        </LinearGradient>
    </View>)
  }
}

const styles = StyleSheet.create({

})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ChatRoom)