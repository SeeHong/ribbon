import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, ScrollView, TextInput, Dimensions, FlatList, TouchableOpacity, Alert} from "react-native";
import {container,textInput,text,absolute} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import HeaderColor from './components/HeaderColor';
import {profile,profile_images} from '../template';
import {Icon} from 'react-native-elements';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';

const options = {
    title: strings('select_image'),
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

class EditProfile extends Component {

    constructor(props) {
        super(props);
        let _profile_images=[];
        for(let x=0; x<9;x++){
            if(profile_images[x]){
                _profile_images[x]={uri: profile_images[x]};
            }else{
                _profile_images[x]=1;
            }
        }
        this.state = {
            profile_images: _profile_images
        }
        this.renderPhoto=this.renderPhoto.bind(this);
        this.deleteImage=this.deleteImage.bind(this);
        this.uploadImage=this.uploadImage.bind(this);
        this.updateProfile=this.updateProfile.bind(this);
    }

    deleteImage(){
        Alert.alert('', strings('delete_image')+"?", [
            {text: 'OK', onPress:()=>{}},
            {text: strings('cancel')}
        ])
    }

    renderInterest(){
        return profile.interest.map(element=>(<View style={styles.interestBadge} key={element}>
        <Text style={text.white}>{element}</Text>
        </View>))
    }

    renderPersonalities(){
        return profile.personality.map(element=>(<View style={styles.personalityBadge} key={element}>
        <Text style={text.white}>{element}</Text>
        </View>))
    }

    uploadImage(){
        ImagePicker.showImagePicker(options, (response) => {
          if (response.didCancel) {
            // ToastAndroid.show('Cancelled', ToastAndroid.SHORT);
          } else if (response.error) {
            // ToastAndroid.show(response.error, ToastAndroid.SHORT);
          } else {
            let uri= Platform.OS === "android" ? response.uri : response.uri.replace("file://", "");
            Image.getSize(uri, (width, height) => {
                if(width > 680 || height > 680){
                    ImageResizer.createResizedImage(uri, 680, 680, "JPEG", 80, 0, null).then(resize_img =>{
                        response.uri = resize_img.uri;
                        response.name = resize_img.name;
                        // fileUploadAjax({
                        //     data: {image: response, id: this.state.id},
                        //     url: "merchant/update_avatar",
                        //     success: result=>{
                        //       this.props.setMerchant(result.data);
                        //     },
                        //     isShowAlert: true
                        // });
                    }).catch((err) => {
                        Alert.alert('Fail to resize', err.toString());
                    });
                }else{
                //   fileUploadAjax({
                //       data: {image: response, id: this.state.id},
                //       url: "merchant/update_avatar",
                //       success: result=>{
                //         this.props.setMerchant(result.data);
                //       },
                //       isShowAlert: true
                //   });
                }
            });
          }
        });
    }

    renderPhoto({item, index}){
        if(item==1){
            return(<TouchableOpacity onPress={this.uploadImage}
            style={[container.center, {height: ((Dimensions.get('window').width*0.9)/3)-3.3, width: ((Dimensions.get('window').width*0.9)/3)-3.3, backgroundColor: getColor('GREY_LIGHTEN_4'), marginRight: (index+1)%3==0?0:5}]}>
                <Image source={require('../assets/add.png')} style={{height: 40, width: 40}}/>
            </TouchableOpacity>)
        }else{
            return (<View style={{height: ((Dimensions.get('window').width*0.9)/3)-3.3, width: ((Dimensions.get('window').width*0.9)/3)-3.3, marginRight: (index+1)%3==0?0:5}}>
                <TouchableOpacity style={styles.deleteImageButton} onPress={this.deleteImage}>
                    <Icon name="minus-circle" color={getColor('white')} type="feather" size={12}/>
                </TouchableOpacity>
                <Image source={item} 
                resizeMode="contain"
                style={{height: ((Dimensions.get('window').width*0.9)/3)-3.3, width: ((Dimensions.get('window').width*0.9)/3)-3.3}} />
            </View>
            )
        }
       
    }

    renderProfilePhotoShow(){
        return (<FlatList
            data={this.state.profile_images}
            numColumns={3}
            renderItem={this.renderPhoto}
        />)
    }

    updateProfile(){
        Alert.alert('', strings('profile_updated'), [
            {text: "OK", onPress: ()=>this.props.navigation.goBack()}
        ])
    }


  render() {
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_4')}]}>
        <HeaderColor title={strings('edit_profile')} renderBackButton={true} navigation={this.props.navigation} 
        renderRightAction={true} rightButtonName="save" rightButtonType="feather" rightButtonAction={this.updateProfile}/>
        <ScrollView style={[container.full, {paddingHorizontal: '5%', backgroundColor: getColor('white')}]} contentContainerStyle={container.column}>
            {this.renderProfilePhotoShow()}
            <View style={[container.row, styles.containerColumn]}>
                <View style={styles.left}>
                    <Text>{strings('name')}</Text>
                </View>
                <View style={styles.right}>
                    <TextInput
                    style={textInput.normal}
                    value={profile.name}
                    />
                </View>
            </View>
            <View style={[container.row, styles.containerColumn]}>
                <View style={styles.left}>
                    <Text>{strings('d_o_b')}</Text>
                </View>
                <View style={styles.right}>
                    <TextInput
                    style={textInput.normal}
                    value={profile.date_of_birth}
                    />
                </View>
            </View>
            <View style={[container.row, styles.containerColumn]}>
                <View style={styles.left}>
                    <Text>{strings('gender')}</Text>
                </View>
                <View style={styles.right}>
                    <TextInput
                    style={textInput.normal}
                    value={strings(profile.gender)}
                    />
                </View>
            </View>
            <View style={[container.row, styles.containerColumn]}>
                <View style={styles.left}>
                    <Text>{strings('about_me')}</Text>
                </View>
                <View style={styles.right}>
                    <TextInput
                    style={textInput.normal}
                    value={profile.description}
                    />
                </View>
            </View>
            <View style={[container.row, styles.containerColumn]}>
                <View style={styles.left}>
                    <Text>{strings('job_title')}</Text>
                </View>
                <View style={styles.right}>
                    <TextInput
                    style={textInput.normal}
                    value={profile.job}
                    />
                </View>
            </View>
            <View style={[container.row, styles.containerColumn]}>
                <View style={styles.left}>
                    <Text>{strings('education')}</Text>
                </View>
                <View style={styles.right}>
                    <TextInput
                    style={textInput.normal}
                    value={profile.education}
                    />
                </View>
            </View>
            <View style={[container.row, styles.containerColumn]}>
                <View style={styles.left}>
                    <Text>{strings('location')}</Text>
                </View>
                <View style={styles.right}>
                    <TextInput
                    style={textInput.normal}
                    value={profile.address}
                    />
                </View>
            </View>
            <View style={[container.column, {marginVertical: 10}]}>
                <Text style={text.title}>{strings('my_interest')}</Text>
                <View style={[container.row, {flexWrap:"wrap", marginVertical: 10}]}>
                {this.renderInterest()}
                </View>
            </View>
            <View style={[container.column, {marginBottom: 10}]}>
                <Text style={text.title}>{strings('my_personality')}</Text>
                <View style={[container.row, {flexWrap:"wrap", marginVertical: 10}]}>
                {this.renderPersonalities()}
                </View>
            </View>
        </ScrollView>
    </View>)
  }
}

const styles = StyleSheet.create({
    containerColumn: {paddingVertical: 5, borderBottomWidth: 0.5, borderColor: getColor('GREY')},
    left: {flex: 1},
    right: {flex: 3},
    interestBadge: {backgroundColor: getColor('BLUE_LIGHTEN_1'), padding: 5, marginRight: 5, borderRadius: 5, marginBottom: 5},
    personalityBadge: {backgroundColor: getColor('GREEN'), padding: 5, marginRight: 5, borderRadius: 5, marginBottom: 5},
    deleteImageButton: {position: "absolute",right: 5, top: 5, zIndex: 10, backgroundColor: getColor("DANGER_COLOR"), height: 20, width: 20, alignItems: "center", justifyContent:"center", borderRadius: 20}
})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile)