import React, { Component } from 'react';
import { View, Text, Image, StyleSheet,ScrollView, TouchableOpacity, Dimensions, FlatList, UIManager, LayoutAnimation} from "react-native";
import {container, text} from '../styles';
import { connect } from 'react-redux';
import {Icon} from 'react-native-elements';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import {Header} from 'react-navigation-stack';
import {ribbons, events} from '../template';
import RibbonsComponent from './components/RibbonsComponent';
import RibbonButton from './components/RibbonButton';
import WebViewModal from './components/WebViewModal';

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      ads_banner: "https://scontent.fkul10-1.fna.fbcdn.net/v/t1.0-9/107556380_2727002080885435_2590979674147368814_o.jpg?_nc_cat=102&_nc_sid=dd9801&_nc_eui2=AeE9qe6AfYHRlTq9ld1e1zcd5oFor9fBCUzmgWiv18EJTMmeJuIohEvNUCra8Ju5ado&_nc_oc=AQmtfX26wsKpg1B4KFCxv91K-j4__zaTxMsTiYuP2t9JGXVzw6m6TFwB9lIgqwmZ-dg&_nc_ht=scontent.fkul10-1.fna&oh=cbd492a59be9dfd970274b9f09091254&oe=5F2F1D70",
      screen: 0,
      webview_visible: false,
      webview_url: ""
    }
    this.renderItem=this.renderItem.bind(this);
    this.ListFooterComponent=this.ListFooterComponent.bind(this);
    this.loadMore=this.loadMore.bind(this);
    this.renderEvent=this.renderEvent.bind(this);
  }

  componentDidMount(){
  }

  loadMore(){

  }

  switchScreen(screen){
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    this.setState({screen: screen});
  }

  ListFooterComponent(){
    return (<View style={[container.center, {marginVertical: 10}]}>
      <RibbonButton style={{paddingHorizontal: 20}} name={strings('show_more_ribbons')} onPress={this.loadMore}/>
    </View>)
  }

  renderEvent({item}){
    return (<TouchableOpacity onPress={()=>this.setState({webview_visible: true, webview_url: item.url})} style={{marginBottom: 10}}>
      <Image source={{uri: item.img}} style={{height: (Dimensions.get('window').width*0.9)/2, width: Dimensions.get('window').width*0.9, marginHorizontal: '5%'}} />
    </TouchableOpacity>)
  }

  renderContent(){
    if(this.state.screen==0){
      return (<ScrollView style={container.full} contentContainerStyle={container.column}>
        <TouchableOpacity onPress={()=>this.setState({webview_visible: true, webview_url: events[0].url})}>
          <Image source={{uri: events[0].img}} style={{height: (Dimensions.get('window').width*0.9)/2, width: Dimensions.get('window').width*0.9, marginHorizontal: '5%'}} />
        </TouchableOpacity>
        <View style={[container.column, {marginVertical: 15, paddingHorizontal: '5%'}]}>
          <Text style={[text.title, {marginBottom: 15}]}>{strings('home_activity')}, {this.props.App.Auth.name}?</Text>
          <FlatList
          data={this.props.App.Activity.activities}
          renderItem={this.renderItem}
          horizontal={true}
          keyExtractor={item => item.id.toString()}
          />
          <Text style={[text.title, {marginBottom: 5, marginTop: 15}]}>{strings('introducing')}</Text>
          <Text style={[text.title, {marginBottom: 5}]}>{strings('ribbon_soul_partner')}</Text>
          <Text style={[text.text, {marginBottom: 5}]}>{strings('home_activity_1')}</Text>

          <FlatList
          data={ribbons}
          renderItem={({item, index})=>(<RibbonsComponent usableWidth={0.9} ribbon={item} index={index} onPress={()=>this.props.navigation.navigate('RibbonProfile')}/>)}
          numColumns={2}
          ListFooterComponent={this.ListFooterComponent}
          keyExtractor={item => item.id.toString()}
          />
        </View>
      </ScrollView>)
    }
    return (<FlatList
      data={events}
      renderItem={this.renderEvent}
      keyExtractor={item => item.id.toString()}
      />);
  }

  renderItem({item, index}){
    return (<TouchableOpacity style={[container.column, container.card, {width: Dimensions.get('window').width/3, marginRight: index==this.props.App.Activity.activities.length-1?0:15, borderRadius: 10, marginBottom: 15, backgroundColor: getColor('white')}]}>
      <Image source={{uri:item.img}} style={{height: Dimensions.get('window').width/3, width:Dimensions.get('window').width/3, borderTopLeftRadius: 10, borderTopRightRadius: 10}} />
      <Text style={[text.text, {textAlign: 'center', paddingVertical: 10}]}>{item.text}</Text>
    </TouchableOpacity>)
  }


  render() {
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_5')}]}>
      <View style={[container.center, {height: Header.HEIGHT}]}>
        <Text style={text.title}>{strings('ribbon')}</Text>
      </View>

      <TouchableOpacity onPress={()=>this.props.navigation.navigate('Search')}
      style={[container.card, container.row, {borderRadius: 5, padding: 10, marginHorizontal: '5%', backgroundColor: getColor('white')}]}>
        <View style={{flex: 1}}>
          <Icon name="search" type="feather" color={getColor('GREY')}/>
        </View>
        <View style={{flex: 4}}>
          <Text style={[text.text, {color: getColor('GREY')}]}>{strings('search')}</Text>
        </View>
      </TouchableOpacity>

      <View style={[container.row, {paddingHorizontal: '5%', marginVertical: 15}]}>
        <View style={[container.row, {flex: 1}]}>
          <TouchableOpacity onPress={()=>this.switchScreen(0)} style={[styles.headerIcon, {borderBottomColor: getColor('ELEGANT_COLOR'), borderBottomWidth: this.state.screen==0?1:0}]}>
            <Text>{strings('popular')}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>this.switchScreen(1)} style={[styles.headerIcon, {borderBottomColor: getColor('ELEGANT_COLOR'), borderBottomWidth: this.state.screen==1?1:0}]}>
            <Text>{strings('event')}</Text>
          </TouchableOpacity>
        </View>
        <View style={[container.row, {flex: 1, justifyContent: "flex-end"}]}>
          <TouchableOpacity style={styles.headerIcon}>
            <Icon name="settings" type="feather" size={20}/>
          </TouchableOpacity>
          <TouchableOpacity onPressIn={()=>this.props.navigation.navigate('SelectDate')} style={styles.headerIcon}>
            <Text>{strings('dates')}</Text>
          </TouchableOpacity>
        </View>
      </View>

      {this.renderContent()}
      <WebViewModal visible={this.state.webview_visible} 
      url={this.state.webview_url} 
      closeModal={()=>this.setState({webview_visible:false})}/>
    </View>)
  }
}

const styles = StyleSheet.create({
  headerIcon: {paddingHorizontal: 10, paddingVertical: 5}
})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)