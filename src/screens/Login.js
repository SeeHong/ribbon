import React, { Component } from 'react';
import { LayoutAnimation, View, Text, Image, Dimensions, ScrollView, TouchableOpacity, TextInput, UIManager } from "react-native";
import {container, button, text, textInput, absolute} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import Loading from './components/Loading';
import {Icon} from 'react-native-elements';
import {ajax} from '../ajax';
import {setIdentity} from "../redux/actions/AuthActions";
import {validateRequired, validatePassword, validateEmail, validateConfirmPassword} from '../validation';
import LinearGradient from 'react-native-linear-gradient';
import DeviceInfo from 'react-native-device-info';
import { getUniqueId, getManufacturer } from 'react-native-device-info';
import CookieManager from 'react-native-cookies';
import {setCookies} from '../redux/actions/AuthActions';
import {APP_FILE_URL} from '../config';

UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      screen: 0,
      username:"",
      password:"",
    };
    this.facebookLogin=this.facebookLogin.bind(this);
    this.phoneLogin=this.phoneLogin.bind(this);
  }

  componentDidMount(){
    CookieManager.get(APP_FILE_URL)
    .then((cookies) => {
      this.props.setCookies(cookies);
    });
  }

  facebookLogin(){
    LoginManager.logInWithPermissions(["public_profile", "email"]).then(
      (result)=>{
        if(!result.isCancelled){
          AccessToken.getCurrentAccessToken().then(data=>{
            this.props.navigation.navigate('LoginAuthentication');
          })
        }
      },
      error=>{
        console.log(error);
      }
    )
  }

  phoneLogin(){
    this.props.navigation.navigate('Register');
  }

  renderContent(){
    if(this.state.screen==0){
      return (<View style={[container.column, {paddingHorizontal: 40, flex: 1, justifyContent: 'space-around'}]}>
        <View style={[container.column, {marginBottom: '10%'}]}>
          <TouchableOpacity style={[button.facebook, {marginBottom: 10}]}>
            <Icon name="facebook-square" type="font-awesome" color={getColor('white')} />
            <Text style={[button.primary_text, {marginLeft: 10}]}>{strings("facebook_continue")}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>this.props.navigation.navigate('Register', {type:true})} style={[button.white_outline_round, {marginBottom: 10}]}>
            <Text style={[button.primary_text]}>{strings("log_in_with_phone_number")}</Text>
          </TouchableOpacity>
          {/* <View style={[container.row, container.center]}>
            <Text style={text.white}>{strings('existing_user')}? </Text>
            <TouchableOpacity onPress={()=>this.setState({screen: 1})}>
              <Text style={text.link}>{strings('log_in')}</Text>
            </TouchableOpacity>
          </View> */}
        </View>
        <Text style={text.white}>{strings('login_text_3')}</Text>
      </View>)
    }
    if(this.state.screen==1){
      return (<View style={[container.column, {paddingHorizontal: 40, flex: 1, justifyContent: 'space-around'}]}>
        <View style={container.column}>
          <TouchableOpacity style={[button.facebook, {marginBottom: 10}]}>
            <Icon name="facebook-square" type="font-awesome" color={getColor('white')} />
            <Text style={[button.primary_text, {marginLeft: 10}]}>{strings("facebook_login")}</Text>
          </TouchableOpacity>
          <View style={container.row}>
            <View style={{borderBottomWidth: 1, borderColor: getColor('white'), flex: 1}}/>
            <Text style={[text.white, {fontSize: 16, paddingHorizontal: 15}]}> {strings('or')} </Text>
            <View style={{borderBottomWidth: 1, borderColor: getColor('white'), flex: 1}}/>
          </View>
          <TextInput
          value={this.state.username}
          onChangeText={text=>this.setState({username:text})}
          style={[textInput.normal, {marginTop: 10}]}
          placeholder={strings('username')}
          keyboardType="default"
          ref={ref=>this.username=ref}
          returnKeyType="next"
          placeholderTextColor={getColor('GREY')}
          onSubmitEditing={()=>this.password.focus()}
          />
          <TextInput
          value={this.state.password}
          onChangeText={text=>this.setState({password:text})}
          style={[textInput.normal, {marginTop: 10}]}
          placeholder={strings('password')}
          keyboardType="default"
          ref={ref=>this.password=ref}
          secureTextEntry={true}
          returnKeyType="done"
          placeholderTextColor={getColor('GREY')}
          onSubmitEditing={this.login}
          />
          <View style={[container.row, {marginVertical: 10}]}>
            <Text style={text.white}>{strings('login_text_4')} </Text>
            <TouchableOpacity>
              <Text style={text.link}>{strings('here')}</Text>
            </TouchableOpacity>
            <Text style={text.white}> {strings('login_text_5')}</Text>
          </View>
          <TouchableOpacity style={[button.white_outline_round, {marginBottom: 15}]} onPress={this.login}>
            <Text style={[button.primary_text]}>{strings("log_in")}</Text>
          </TouchableOpacity>
        </View>
        <Text style={[text.white, {marginTop: 25}]}>{strings('login_text_3')}</Text>
      </View>)
    }
  }
  
  render() {
    return (<LinearGradient style={[container.full]} colors={[getColor('APP_1'), getColor('APP_3'), getColor('APP_2')]}>
      <ScrollView style={container.full} contentContainerStyle={[container.column]} keyboardShouldPersistTaps="always">
        <View style={[container.center, container.column, {height: (Dimensions.get('window').width)*3/5, width: Dimensions.get('window').width}]}>
          <Image source={require('../assets/logo_secondary.png')} resizeMode="contain" style={{height: Dimensions.get('window').width/3, width: Dimensions.get('window').width/3}}/>
          <Text style={text.login_title_1}>{strings('login_text_1')}</Text>
          <Text style={text.login_title_2}>{strings('login_text_2')}</Text>
          {this.state.screen==1?<TouchableOpacity style={[absolute.top, {right: 10, top: 10}]} onPress={()=>this.setState({screen: 0})}>
            <Text style={[text.white, {fontSize: 16}]}>{strings('sign_in')}</Text>
          </TouchableOpacity>:null}
        </View>
        {this.renderContent()}
      </ScrollView>
      <Loading visible={this.state.loading} />
    </LinearGradient>)
  }
}

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setIdentity: data=>dispatch(setIdentity(data)),
    setCookies: data=>dispatch(setCookies(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)