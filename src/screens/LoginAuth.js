import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity, ScrollView, Alert} from "react-native";
import {container,text,textInput} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import {Icon} from 'react-native-elements';
import {Header} from 'react-navigation-stack';
import RibbonButton from './components/RibbonButton';
import {ajax} from '../ajax';
import OTPComponent from './components/OTPComponent';
import {setIdentity} from '../redux/actions/AuthActions';

class LoginAuth extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username:"",
      phone_number: "1234567893",
      screen: props.navigation.getParam('type')==true?0:2,
      timer: 60
    }
    this.register=this.register.bind(this);
    this.requestOTP=this.requestOTP.bind(this);
    this.submitOTP=this.submitOTP.bind(this);
  }

  register(){
    this.props.navigation.navigate('Setup');
  }

  requestOTP(){
    this.setState({loading:true, timer: 60}, ()=>{
      clearInterval(this.timer);
      ajax({
        url: "member/auth/request-verification",
        data: {	contact: "+60"+this.state.phone_number},
        success: result=>{
          this.timer=setInterval(()=>{
            if(this.state.timer>0){
              this.setState({timer: this.state.timer-1});
            }else{
              clearInterval(this.timer);
            }
          }, 1000)
          Alert.alert(result.code, result.message, [
            {text:"OK", onPress:()=>this.setState({screen: 1})}
          ])
        },
        isShowAlert: true
      })
    })
  }

  submitOTP(otp){
    clearInterval(this.timer);
    this.setState({loading:true, timer: 60}, ()=>{
      ajax({
        url: "member/auth/verify",
        data: {	
          contact: "+60"+this.state.phone_number,
          code: otp,
          deviceCode: "f5UrQac7amTH"
        },
        success: result=>{
          this.props.setIdentity(result);
          this.props.navigation.navigate('Setup');
        },
        isShowAlert: true
      })
    })
  }

  render() {
    if(this.state.screen==0){
      return (<View style={[container.full, {backgroundColor: getColor('white')}]}>
        <View style={[styles.header, container.row, {paddingHorizontal: 30}]}>
          <TouchableOpacity onPress={()=>this.props.navigation.navigate('Login')}>
            <Icon name="chevron-left" type="feather" color={getColor("ELEGANT_COLOR")} />
          </TouchableOpacity>
        </View>
        <View style={[container.full, container.column]}>
          <View style={[container.full, container.column, {paddingHorizontal: 40}]}>
            <Text style={text.register_title_1}>{strings('my_phone_number_is')}</Text>
            <View style={[container.row, {alignItems: "flex-end"}]}>
              <Text style={[text.muted, {flex: 1, borderBottomColor: getColor('ELEGANT_COLOR'), borderBottomWidth: 0.5, paddingBottom: 10, textAlign: "center"}]}>MY +60</Text>
              <TextInput
              value={this.state.phone_number}
              onChangeText={text=>this.setState({phone_number:text})}
              style={[textInput.normal, {flex: 4, borderBottomColor: getColor('ELEGANT_COLOR'), borderBottomWidth: 0.5, backgroundColor: getColor('white')}]}
              placeholder={strings('phone_number')}
              keyboardType="default"
              ref={ref=>this.phone_number=ref}
              returnKeyType="next"
              placeholderTextColor={getColor('GREY')}
              onSubmitEditing={this.requestOTP}
              />
            </View>
            <View style={[container.row, {marginVertical: 10}]}>
                <Text style={[text.text, {marginLeft: 10}]}>{strings("verification_text")}</Text>
            </View>
            <RibbonButton style={{marginVertical: 20}} name={strings("continue")} onPress={this.requestOTP} />
          </View>
        </View>
    </View>)
    }
    if(this.state.screen==1){
      return (<View style={[container.full, {backgroundColor: getColor('white')}]}>
        <View style={[styles.header, container.row, {paddingHorizontal: 30}]}>
          <TouchableOpacity onPress={()=>this.props.navigation.navigate('Login')}>
            <Icon name="chevron-left" type="feather" color={getColor("ELEGANT_COLOR")} />
          </TouchableOpacity>
        </View>
        <View style={[container.full, container.column]}>
          <View style={[container.full, container.column, {paddingHorizontal: 40}]}>
            <Text style={text.register_title_1}>{strings('my_verification_code_is')}</Text>
            <Text style={[text.text, {marginBottom: 20}]}>+60{this.state.phone_number}</Text>
            <OTPComponent otp={this.submitOTP} />
            <View style={[container.center, {marginTop: 15}]}>
              {this.state.timer==0?<TouchableOpacity onPress={this.requestOTP} style={[container.center]}>
                <Text style={[text.text, {marginLeft: 10}]}>{strings("resend_otp")}</Text>
              </TouchableOpacity>:<Text style={text.text}>{this.state.timer}</Text>}
            </View>
          </View>
        </View>
    </View>)
    }
    return null;
  }
}

const styles = StyleSheet.create({
    header: {height: Header.HEIGHT},
})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setIdentity: data=>dispatch(setIdentity(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginAuth)