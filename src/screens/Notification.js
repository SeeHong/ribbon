import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, FlatList} from "react-native";
import {container} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import {text} from '../styles';
import {notification} from '../template';
import NotificationComponent from './components/NotificationComponent';

class Notification extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount(){
  }


  render() {
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_4')}]}>
      <View style={{padding: '10%', borderBottomWidth: 0.5, borderColor: getColor('GREY'), marginBottom: 15}}>
        <Text style={[text.title, {fontSize: 28}]}>{strings('notification')}</Text>
      </View>
      <FlatList
      data={notification}
      renderItem={({item, index})=>(<NotificationComponent chat={item} index={index} onPress={()=>{}}/>)}
      style={{paddingHorizontal: '5%'}}
      keyExtractor={item => item.id.toString()}
      />
    </View>)
  }
}

const styles = StyleSheet.create({

})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Notification)