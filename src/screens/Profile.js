import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Dimensions, ScrollView} from "react-native";
import {container, absolute} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import {text} from '../styles';
import {Header} from 'react-navigation-stack';
import {Icon} from 'react-native-elements';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import {profile, profile_images} from '../template';
import BadgeGenderAge from './components/BadgeGenderAge';
import BadgeHoroscope from './components/BadgeHoroscope';
import LinearGradient from 'react-native-linear-gradient';

class Profile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      index: 0
    }
    this.renderProfileImages=this.renderProfileImages.bind(this);
  }

  renderProfileImages({item}){
    return (<Image source={{uri: item}} style={{height: Dimensions.get('window').width*0.75, width: Dimensions.get('window').width}} />)
  }

  renderInterest(){
    return profile.interest.map(element=>(<View style={styles.interestBadge} key={element}>
      <Text style={text.white}>{element}</Text>
    </View>))
  }

  renderPersonalities(){
    return profile.personality.map(element=>(<View style={styles.personalityBadge} key={element}>
      <Text style={text.white}>{element}</Text>
    </View>))
  }

  render() {
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_5')}]}>
      <View style={styles.header}>
        <Text style={[text.title]}>{this.props.App.Auth.name}{strings('profile_title')}</Text>
        <TouchableOpacity style={styles.settingsIcon} onPress={()=>this.props.navigation.navigate('Settings')}>
          <Icon name="settings" type="feather" size={20} color={getColor('ELEGANT_COLOR')}/>
        </TouchableOpacity>
      </View>
      <ScrollView style={container.full} contentContainerStyle={container.column}>
        <View>
          <Carousel
          ref={c => { this.carousel = c; }}
          data={profile_images}
          renderItem={this.renderProfileImages}
          sliderWidth={Dimensions.get('window').width}
          itemWidth={Dimensions.get('window').width}
          loop={false}
          autoplay={false}
          onSnapToItem={(index) => this.setState({ index: index }) }
          />
          <Pagination
          dotsLength={profile_images.length}
          activeDotIndex={this.state.index}
          containerStyle={[container.center, absolute.bottom, {left: 0, right: 0, padding: 0}]}
          tappableDots={true}
          dotColor={getColor('white')}
          inactiveDotColor={getColor('GREY')}
          inactiveDotOpacity={0.8}
          inactiveDotScale={0.8}
          carouselRef={this.carousel}
          tappableDots={!!this.carousel}
          />
        </View>
        <View style={{paddingHorizontal: '5%', paddingVertical: 15}}>
          <View style={[container.row, {marginBottom: 5}]}>
            <Text style={text.title}>{profile.name}</Text>
            <BadgeGenderAge age={profile.age} gender={profile.gender} style={{marginHorizontal: 5}}/>
            <BadgeHoroscope horoscope={profile.horoscope} style={{marginHorizontal: 5}}/>
          </View>
          <Text style={text.text}>{strings('in')} {profile.address}</Text>
          <Text style={[text.text, {marginVertical: 5}]}>{profile.job}</Text>
          <Text style={[text.text]}>{profile.description}</Text>
          
          <View style={[container.card, styles.bookingContainer]}>
            <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={[getColor('APP_1'), getColor('APP_2')]} style={[container.row, styles.bookingHeader]}>
              <Text style={text.white}>{strings('my_booking')}</Text>
              <TouchableOpacity style={container.row}>
                <Text style={text.white}>{strings('view_my_booking_history')}</Text>
                <Icon name="chevron-right" type="feather" size={11} style={{marginLeft: 5}} color={getColor('white')} />
              </TouchableOpacity>
            </LinearGradient>
            <View style={container.row}>
              <TouchableOpacity style={styles.bookingButton}>
                <Image source={require('../assets/1.png')} style={{height: 30, width: 30}} />
                <Text style={text.text}>{strings('reserved')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bookingButton}>
                <Image source={require('../assets/2.png')} style={{height: 30, width: 30}} />
                <Text style={text.text}>{strings('pending')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bookingButton}>
                <Image source={require('../assets/3.png')} style={{height: 30, width: 30}}/>
                <Text style={text.text}>{strings('history')}</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bookingButton}>
              <Image source={require('../assets/4.png')} style={{height: 30, width: 30}}/>
                <Text style={text.text}>{strings('cancelled')}</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={[container.column, {marginVertical: 10}]}>
            <Text style={text.title}>{strings('my_interest')}</Text>
            <View style={[container.row, {flexWrap:"wrap", marginVertical: 10}]}>
              {this.renderInterest()}
            </View>
          </View>
          <View style={[container.column, {marginVertical: 10}]}>
            <Text style={text.title}>{strings('my_personality')}</Text>
            <View style={[container.row, {flexWrap:"wrap", marginVertical: 10}]}>
              {this.renderPersonalities()}
            </View>
          </View>
        </View>
      </ScrollView>
    </View>)
  }
}

const styles = StyleSheet.create({
  header: {height: Header.HEIGHT, alignItems: 'center', justifyContent: "center", backgroundColor: getColor('white')},
  settingsIcon: {position: "absolute", top: 0, right: 0, zIndex: 1, height: Header.HEIGHT, width: 50, alignItems: 'center', justifyContent: "center"},
  interestBadge: {backgroundColor: getColor('BLUE_LIGHTEN_1'), padding: 5, marginRight: 5, borderRadius: 5, marginBottom: 5},
  personalityBadge: {backgroundColor: getColor('GREEN'), padding: 5, marginRight: 5, borderRadius: 5, marginBottom: 5},
  bookingHeader: {justifyContent: "space-between", padding: 10, borderTopLeftRadius: 10, borderTopRightRadius: 10},
  bookingContainer: {backgroundColor: getColor('white'), marginVertical: 15, borderRadius: 10},
  bookingButton: {flex: 1, flexDirection: "column", alignItems: "center", padding: 10}
})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)