import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, Alert} from "react-native";
import {container, text} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import HeaderColor from './components/HeaderColor';
import {Avatar, Slider } from 'react-native-elements';
import { Switch } from 'react-native-paper';

class RibbonPreferences extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
    this.updatePreferences=this.updatePreferences.bind(this);
  }

  componentDidMount(){
  }

  updatePreferences(){
    Alert.alert('', strings('preferences_updated'), [
        {text: "OK", onPress: ()=>this.props.navigation.goBack()}
    ])
  }

  render() {
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_4')}]}>
        <HeaderColor title={strings('ribbon_preferences')} renderBackButton={true} navigation={this.props.navigation} 
        renderRightAction={true} rightButtonName="save" rightButtonType="feather" rightButtonAction={this.updatePreferences}/>
        <View style={[container.column, container.full, {paddingHorizontal: '5%', marginVertical: 15}]}>
          <Text style={[text.title, {marginBottom: 5}]}>{strings('your_settings')}</Text>
          <Text style={[text.text, {marginBottom: '15%'}]}>{strings('settings_text_1')}</Text>
          <View style={[container.column, container.card, {backgroundColor: getColor("GREY_LIGHTEN_4"), borderRadius: 10}]}>
            <View style={[container.row, {padding: 15, borderBottomWidth: 0.5, borderColor: getColor('GREY')}]}>
              <Text style={[text.text, {flex: 1}]}>{strings("location")}</Text>
              <Switch value={false} onValueChange={()=>{}} />
            </View>
            <View style={[container.row, {padding: 15, borderBottomWidth: 0.5, borderColor: getColor('GREY')}]}>
              <Text style={[text.text, {flex: 1}]}>{strings("distance")}</Text>
              <View style={container.full}>
                <Slider
                value={0}
                minimumValue={0}
                maximumValue={100}
                onValueChange={(value) => {}}
                />
              </View>
            </View>
            <View style={[container.row, {padding: 15}]}>
              <Text style={[text.text, {flex: 1}]}>{strings("age_range")}</Text>
              <View style={container.full}>
                <Slider
                value={0}
                minimumValue={0}
                maximumValue={100}
                onValueChange={(value) => {}}
                />
              </View>
            </View>
          </View>
        </View>
    </View>)
  }
}

const styles = StyleSheet.create({

})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RibbonPreferences)