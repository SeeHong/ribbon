import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, ScrollView, Dimensions, TouchableOpacity, FlatList} from "react-native";
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import {Icon} from 'react-native-elements';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import {ribbon} from '../template';
import {absolute, container, text, overlay} from '../styles';
import BadgeGenderAge from './components/BadgeGenderAge';
import BadgeHoroscope from './components/BadgeHoroscope';
import ReviewComponent from './components/ReviewComponent';

class RibbonProfile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      index:0,
      screen: 0,
      audio: false,
    }
    this.renderProfileImages=this.renderProfileImages.bind(this);
    this.renderReview=this.renderReview.bind(this);
  }

  renderProfileImages({item}){
    return (<Image source={{uri: item}} style={{height: Dimensions.get('window').width*0.75, width: Dimensions.get('window').width}} resizeMode="cover"/>)
  }

  renderInterest(){
    return ribbon.interest.map(element=>(<View style={styles.interestBadge} key={element}>
      <Text style={text.white}>{element}</Text>
    </View>))
  }

  renderPersonalities(){
    return ribbon.personality.map(element=>(<View style={styles.personalityBadge} key={element}>
      <Text style={text.white}>{element}</Text>
    </View>))
  }

  renderReview({item}){
    return (<View>

    </View>)
  }

  renderScreen(){
    if(this.state.screen==0){
      return (<View style={container.column}>
        <View style={[container.column, {marginVertical: 10}]}>
          <Text style={text.title}>{strings('my_interest')}</Text>
          <View style={[container.row, {flexWrap:"wrap", marginVertical: 10}]}>
            {this.renderInterest()}
          </View>
        </View>
        <View style={[container.column, {marginVertical: 10}]}>
          <Text style={text.title}>{strings('my_personality')}</Text>
          <View style={[container.row, {flexWrap:"wrap", marginVertical: 10}]}>
            {this.renderPersonalities()}
          </View>
        </View>
      </View>)
    }
    if(this.state.screen==1){
      return (<FlatList
      data={ribbon.reviews}
      renderItem={({item})=><ReviewComponent review={item} />}
      keyExtractor={item => item.id.toString()}
      />)
    }
    return null;
  }

  render() {
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_5')}]}>
    <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={[absolute.top, overlay.black, {left: 10, top: 10, zIndex: 10, height: 40, width: 40, borderRadius: 50, alignItems: 'center', justifyContent: "center"}]}>
      <Icon name="x" type="feather" color={getColor("white")} />
    </TouchableOpacity>
    <ScrollView style={container.full} contentContainerStyle={container.column}>
      <View>
        <Carousel
        ref={c => { this.carousel = c; }}
        data={ribbon.profile_images}
        renderItem={this.renderProfileImages}
        sliderWidth={Dimensions.get('window').width}
        itemWidth={Dimensions.get('window').width}
        loop={false}
        autoplay={false}
        onSnapToItem={(index) => this.setState({ index: index }) }
        />
        <Pagination
        dotsLength={ribbon.profile_images.length}
        activeDotIndex={this.state.index}
        containerStyle={[container.center, absolute.bottom, {left: 0, right: 0, padding: 0}]}
        tappableDots={true}
        dotColor={getColor('white')}
        inactiveDotColor={getColor('GREY')}
        inactiveDotOpacity={0.8}
        inactiveDotScale={0.8}
        carouselRef={this.carousel}
        tappableDots={!!this.carousel}
        />
      </View>
      <View style={{paddingHorizontal: '5%', paddingVertical: 15}}>
        <View style={[container.row, {marginBottom: 5, justifyContent: "space-between"}]}>
          <View style={container.row}>
            <Text style={text.title}>{ribbon.name}</Text>
            <BadgeGenderAge age={ribbon.age} gender={ribbon.gender} style={{marginHorizontal: 5}}/>
            <BadgeHoroscope horoscope={ribbon.horoscope} style={{marginHorizontal: 5}}/>
          </View>
          <TouchableOpacity onPress={()=>this.setState({audio: !this.state.audio})} style={container.row}>
            {!this.state.audio?<Icon name="playcircleo" type="antdesign" size={20} />:<Icon name="pausecircleo" type="antdesign" size={20} />}
          </TouchableOpacity>
        </View>
        <Text style={text.text}>{strings('in')} {ribbon.address} ({ribbon.distance}km)</Text>
        <Text style={[text.text, {marginVertical: 5}]}>{ribbon.job}</Text>
        <Text style={[text.text]}>{ribbon.description}</Text>
        <View style={[container.row]}>
          <TouchableOpacity onPress={()=>this.setState({screen:0})} style={[styles.tab, {backgroundColor: this.state.screen==0?getColor('white'):getColor('GREY_LIGHTEN_2')}]}>
            <Text>{strings('ribbons')}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>this.setState({screen:1})} style={[styles.tab, {backgroundColor: this.state.screen==1?getColor('white'):getColor('GREY_LIGHTEN_2')}]}>
            <Text>{strings('reviews')}</Text>
          </TouchableOpacity>
        </View>
        {this.renderScreen()}
      </View>
    </ScrollView>
    <View style={[container.row, {paddingHorizontal: '5%'}]}>
      <View style={[container.row, {flex: 3}]}>
        <TouchableOpacity style={styles.buttonRibbon}>
          <Text style={text.white}>RM {ribbon.price}/{strings('ribbons')}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonReserve}>
          <Text style={text.white}>{strings('reserve')}</Text>
        </TouchableOpacity>
      </View>
      <View style={[container.row, {flex: 1}]}>
        <TouchableOpacity style={styles.buttonIcon}>
          <Icon name="hearto" type="antdesign" />
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonIcon}>
          <Icon name="dots-three-horizontal" type="entypo" />
        </TouchableOpacity>
      </View>
    </View>
    </View>)
  }
}

const styles = StyleSheet.create({
  interestBadge: {backgroundColor: getColor('BLUE_LIGHTEN_1'), padding: 5, marginRight: 5, borderRadius: 5, marginBottom: 5},
  personalityBadge: {backgroundColor: getColor('GREEN'), padding: 5, marginRight: 5, borderRadius: 5, marginBottom: 5},
  tab: {flex: 1, marginVertical: 15, alignItems: 'center', justifyContent:"center", backgroundColor: getColor('GREY_LIGHTEN_2'), paddingVertical: 15, borderRadius: 10},
  buttonRibbon: {backgroundColor: getColor('RED_LIGHTEN_3'), flex: 1, paddingVertical: 10, justifyContent: "center", alignItems: 'center', borderTopLeftRadius: 10, borderBottomLeftRadius: 10},
  buttonReserve: {backgroundColor: getColor('PINK_LIGHTEN_1'), flex: 1, paddingVertical: 10, justifyContent: "center", alignItems: 'center', borderTopRightRadius: 10, borderBottomRightRadius: 10},
  buttonIcon: {flex: 1}
})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RibbonProfile)