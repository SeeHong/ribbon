import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, FlatList} from "react-native";
import {container} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import {text} from '../styles';
import {ribbons} from '../template';
import RibbonsComponent from './components/RibbonsComponent';

class Saved extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount(){
  }


  render() {
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_5')}]}>
      <View style={{padding: '10%', borderBottomWidth: 0.5, borderColor: getColor('GREY'), marginBottom: 15}}>
        <Text style={[text.title, {fontSize: 28}]}>{strings('saved')}</Text>
      </View>
      <FlatList
      data={ribbons}
      renderItem={({item, index})=>(<RibbonsComponent usableWidth={0.9} ribbon={item} index={index} onPress={()=>this.props.navigation.navigate('RibbonProfile')}/>)}
      numColumns={2}
      style={{paddingHorizontal: '5%'}}
      ListFooterComponent={this.ListFooterComponent}
      keyExtractor={item => item.id.toString()}
      />
    </View>)
  }
}

const styles = StyleSheet.create({

})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Saved)