import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput, FlatList} from "react-native";
import {container, text, textInput} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import {Header} from 'react-navigation-stack';
import {Icon, Avatar} from 'react-native-elements';
import {profile, trending_search} from '../template';

class Search extends Component {

  constructor(props) {
    super(props);
    this.state = {
        search: "",
    }
    this.searchRibbon=this.searchRibbon.bind(this);
    this.renderTrending=this.renderTrending.bind(this);
  }

  componentDidMount(){
  }

  searchRibbon(){

  }

  renderTrending({item, index}){
      return (<TouchableOpacity style={[container.row, {paddingVertical: 10, borderBottomWidth: index+1==trending_search.length?0:0.5, borderColor: getColor('GREY')}]}>
            <View style={[container.center, {width: 30}]}>
                <Text style={text.text}>{index+1}</Text>
            </View>
            <View style={[container.center, {width: 50}]}>
                <Avatar rounded source={{uri: item.img}} imageProps={{resizeMode:"contain"}} size={40}/>
            </View>
            <View style={[container.column, {flex: 1}]}>
                <Text style={text.text}>{item.name}</Text>
                <Text style={text.text}>{item.view}</Text>
            </View>
      </TouchableOpacity>)
  }

  renderRecentSearch(){
    return profile.personality.map(element=>(<View style={styles.recentBadge} key={element}>
        <Text style={text.text}>{element}</Text>
    </View>))
  }

  render() {
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_4')}]}>
        <View style={[container.row, {borderRadius: 5, padding: 10, backgroundColor: getColor('white'), height: Header.HEIGHT}]}>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={[container.center, {height: Header.HEIGHT, width: 50}]}>
                <Icon name="chevron-left" type="feather" color={getColor('ELEGANT_COLOR')}/>
            </TouchableOpacity>
            <View style={{flex: 1}}>
                <TextInput
                value={this.state.search}
                onChangeText={text=>this.setState({search:text})}
                style={[textInput.normal]}
                placeholder={strings('search')}
                keyboardType="default"
                ref={ref=>this.search=ref}
                returnKeyType="done"
                placeholderTextColor={getColor('GREY')}
                onSubmitEditing={this.searchRibbon}
                />
            </View>
            <TouchableOpacity onPress={()=>this.setState({search: ""})} style={[container.center, {height: Header.HEIGHT, width: 50}]}>
                <Icon name="x" type="feather" color={getColor('DANGER_COLOR')}/>
            </TouchableOpacity>
        </View>
        <View style={[container.column, {backgroundColor: getColor('white'), padding: 10, paddingHorizontal: '5%'}]}>
            <Text style={text.title}>{strings('recent_search')}</Text>
            <View style={[container.row, {flexWrap:"wrap", marginVertical: 10}]}>
                {this.renderRecentSearch()}
            </View>
        </View>
        <View style={[container.column, {backgroundColor: getColor('white'), padding: 10, paddingHorizontal: '5%'}]}>
            <View style={[container.row, {justifyContent: "space-between"}]}>
                <Text style={text.title}>{strings('trending_search')}</Text>
                <TouchableOpacity style={container.row}>
                    <Text>{strings('more')}</Text>
                    <Icon name="chevron-right" type="feather" size={18} />
                </TouchableOpacity>
            </View>
            <FlatList
            data={trending_search}
            renderItem={this.renderTrending}
            keyExtractor={item => item.id.toString()}
            />
        </View>
    </View>)
  }
}

const styles = StyleSheet.create({
    recentBadge: {backgroundColor: getColor('GREY_LIGHTEN_3'), padding: 5, marginRight: 5, borderRadius: 5, marginBottom: 5},
})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)