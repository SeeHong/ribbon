import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Dimensions} from "react-native";
import {container} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import HeaderColor from './components/HeaderColor';
import {Calendar} from 'react-native-calendars';
import {text} from '../styles';
import RibbonButton from './components/RibbonButton';

class SelectDate extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
    this.continue=this.continue.bind(this);
  }

  componentDidMount(){
  }

  continue(){
      this.props.navigation.goBack();
  }


  render() {
    
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_4')}]}>
        <HeaderColor renderBackButton={true} navigation={this.props.navigation} />
        <View style={[container.full, container.column]}>
            <View style={{marginBottom: 15, margin: '5%'}}>
                <Text style={[text.title, {fontSize: 28}]}>{strings('select_date_for_your_activity')}</Text>
            </View>
            <View style={[container.card, container.column, {backgroundColor: getColor('white'), marginHorizontal: '5%'}]}>
                <Calendar
                markedDates={{
                    '2020-07-21': {startingDay: true, color: '#50cebb', textColor: 'white'},
                    '2020-07-22': {color: '#70d7c7', textColor: 'white'},
                    '2020-07-23': {color: '#70d7c7', textColor: 'white'},
                    '2020-07-24': {color: '#70d7c7', textColor: 'white'},
                    '2020-07-25': {endingDay: true, color: '#50cebb', textColor: 'white'},
                }}
                markingType={'period'}
                />
            </View>
        </View>
        <View style={container.center}>
            <RibbonButton style={{marginHorizontal: '5%', width: Dimensions.get('window').width*0.8}} name={strings("done")} onPress={this.continue} />
        </View>
    </View>)
  }
}

const styles = StyleSheet.create({

})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectDate)