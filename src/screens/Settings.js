import React, { Component } from 'react';
import { View, Text, Alert, StyleSheet, TouchableOpacity} from "react-native";
import {container, text} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import {Icon} from 'react-native-elements';
import HeaderColor from './components/HeaderColor';
import {APP_VERSION_NAME, PRIVACY_POLICY_URL_EN, ABOUT_US_URL_EN, HELP_URL, TERMS_AND_CONDITIONS_URL_EN} from '../config';
import WebViewModal from './components/WebViewModal';

class Settings extends Component {

  constructor(props) {
    super(props);
    this.state = {
      privary_policy_modal:false,
      term_of_use_modal: false,
      help_modal: false,
      about_modal: false,
    }
    this.renderNavigation=this.renderNavigation.bind(this);
    this.logout=this.logout.bind(this);
  }

  renderNavigation(screen, icon, type, title){
    return (<TouchableOpacity style={[container.row, {borderBottomWidth: 0.5, borderColor: getColor('GREY'), paddingVertical: 10, backgroundColor: getColor('white')}]} onPress={()=>this.props.navigation.navigate(screen)}>
      <View style={[container.center, {flex: 1}]}>
          <Icon size={20} name={icon} type={type} color={getColor("ELEGANT_COLOR")} />
      </View>
      <View style={[{flex: 4}]}>
          <Text style={text.text}>{strings(title)}</Text>
      </View>
    </TouchableOpacity>)
  }

  logout(){
    Alert.alert('', strings("are_you_sure_you_want_to_logout"), [
      {
        text:"OK", onPress: ()=>this.props.navigation.navigate("Login")
      },
      {text: strings('cancel')}
    ])
  }

  render() {
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_4'), justifyContent: 'space-between'}]}>
        <View style={container.column}>
          <HeaderColor title={strings('settings')} renderBackButton={true} navigation={this.props.navigation} 
          renderRightAction={true} rightButtonName="logout" rightButtonType="material-community" rightButtonAction={this.logout} rightButtonColor={getColor("DANGER_COLOR")}/>
          {this.renderNavigation('EditProfile', 'account-edit', 'material-community', 'edit_profile')}
          {this.renderNavigation('RibbonPreferences', 'ribbon', 'material-community', 'ribbon_preferences')}
          {this.renderNavigation('Notification', 'notifications', 'ionicons', 'notification')}
          {this.renderNavigation('BecomeHost', 'cash-usd', 'material-community', 'become_a_ribbon_host')}
          <TouchableOpacity 
          onPress={()=>this.setState({help_modal: true})}
          style={[container.row, {borderBottomWidth: 0.5, borderColor: getColor('GREY'), paddingVertical: 10, backgroundColor: getColor('white')}]}>
            <View style={[container.center, {flex: 1}]}>
                <Icon size={20} name="help-circle-outline" type='material-community' color={getColor("ELEGANT_COLOR")} />
            </View>
            <View style={[{flex: 4}]}>
                <Text style={text.text}>{strings("help")}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>this.setState({about_modal: true})}
          style={[container.row, {borderBottomWidth: 0.5, borderColor: getColor('GREY'), paddingVertical: 10, backgroundColor: getColor('white')}]}>
            <View style={[container.center, {flex: 1}]}>
                <Icon size={20} name={"information-outline"} type={"material-community"} color={getColor("ELEGANT_COLOR")} />
            </View>
            <View style={[{flex: 4}]}>
                <Text style={text.text}>{strings("about_us")}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>this.setState({privary_policy_modal: true})}
          style={[container.row, {borderBottomWidth: 0.5, paddingHorizontal: '10%', borderColor: getColor('GREY'), paddingVertical: 10, backgroundColor: getColor('white')}]}>
            <Text style={text.text}>{strings('privacy_policy')}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>this.setState({term_of_use_modal: true})}
          style={[container.row, {borderBottomWidth: 0.5, paddingHorizontal: '10%', borderColor: getColor('GREY'), paddingVertical: 10, backgroundColor: getColor('white')}]}>
            <Text style={text.text}>{strings('term_of_use')}</Text>
          </TouchableOpacity>
        </View>
        <View style={[container.row, {justifyContent: "center", padding: 10}]}>
          <Text>Version: {APP_VERSION_NAME}</Text>
        </View>
        <WebViewModal visible={this.state.privary_policy_modal} 
        url={PRIVACY_POLICY_URL_EN} 
        title={strings('privacy_policy')}
        closeModal={()=>this.setState({privary_policy_modal:false})}/>
        <WebViewModal visible={this.state.term_of_use_modal} 
        url={TERMS_AND_CONDITIONS_URL_EN} 
        title={strings('term_of_use')}
        closeModal={()=>this.setState({term_of_use_modal:false})}/>
        <WebViewModal visible={this.state.help_modal} 
        url={HELP_URL} 
        title={strings('help')}
        closeModal={()=>this.setState({help_modal:false})}/>
        <WebViewModal visible={this.state.about_modal} 
        url={ABOUT_US_URL_EN}
        title={strings('about_us')} 
        closeModal={()=>this.setState({about_modal:false})}/>
    </View>)
  }
}

const styles = StyleSheet.create({

})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings)