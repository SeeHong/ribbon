import React, { Component } from 'react';
import { Image, TextInput, View, Text, ScrollView, FlatList, TouchableOpacity,Dimensions } from "react-native";
import {container, button, text, line, textInput} from '../styles';
import { connect } from 'react-redux';
import { getColor } from "../colors";
import Loading from './components/Loading';
import {ajax} from '../ajax';
import {strings} from '../translation/i18n';
import {Header} from 'react-navigation-stack';
import {Avatar, Slider, Icon } from 'react-native-elements';
import { Switch } from 'react-native-paper';
import RibbonButton from './components/RibbonButton';
import DatePicker from 'react-native-datepicker';

class Setup extends Component {

    constructor(props) {
      super(props);
      this.state={
        loading: false,
        screen:0,
        name: "",
        dob: "",
        gender: 0,
        email: "",
        activities: [],
        loading: false
      }
      this.continue=this.continue.bind(this);
      this.renderItem=this.renderItem.bind(this);
    }

    continue(){
      if(this.state.screen==0){
        this.setState({loading:true}, ()=>{
          ajax({
            url: "member/me",
            method: "PUT",
            data: {	
              name: this.state.name,
              email: this.state.email,
              gender: this.state.gender,
              birthDate: this.state.birthDate
            },
            success: result=>{
              this.setState({screen: 1})
            },
            complete: ()=>this.setState({loading:false}),
            isShowAlert: true
          })
        })
      }
      if(this.state.screen==1){
        this.setState({screen: 2})
      }
      if(this.state.screen==2){
        this.props.navigation.navigate('Home');
      }
    }

    renderItem({item, index}){
      return (<TouchableOpacity style={[container.column, container.card, {width: Dimensions.get('window').width*0.325, marginRight: index%2?0:Dimensions.get('window').width*0.05, borderRadius: 10, marginBottom: 15, backgroundColor: getColor('white')}]}>
        <Image source={{uri:item.img}} style={{height: Dimensions.get('window').width/3, width:Dimensions.get('window').width*0.325, borderTopLeftRadius: 10, borderTopRightRadius: 10}} />
        <Text style={[text.text, {textAlign: 'center', paddingVertical: 10}]}>{item.text}</Text>
      </TouchableOpacity>)
    }

    renderContent(){
      if(this.state.screen==0){
        return (<View style={[container.column, container.full]}>
          <Text style={text.title}>{strings('my_profile')}</Text>
          <View style={[container.center, {marginBottom: 20, marginTop: 10}]}>
            <Avatar 
            rounded 
            source={require('../assets/user.png')}
            size={80}
            />
          </View>
          <View style={[container.row, {marginBottom: 5}]}>
            <View style={[container.row, {flex: 1, paddingRight: 10}]}>
              <Text style={[text.text]}>{strings('name')}</Text>
            </View>
            <TextInput
            value={this.state.name}
            onChangeText={text=>this.setState({name:text})}
            style={[textInput.normal, {backgroundColor: getColor('GREY_LIGHTEN_4'), flex: 3, textAlign: "center"}]}
            keyboardType="default"
            ref={ref=>this.name=ref}
            returnKeyType="next"
            placeholderTextColor={getColor('GREY')}
            />
          </View>
          <View style={[container.row, {marginBottom: 5}]}>
            <View style={[container.row, {flex: 1, paddingRight: 10}]}>
              <Text style={[text.text]}>{strings('d_o_b')}</Text>
            </View>
            <DatePicker
            style={textInput.normal, {backgroundColor: getColor('GREY_LIGHTEN_4'), flex: 3}}
            customStyles={{dateInput:{borderWidth: 0}}}
            showIcon={false}
            date={this.state.dob}
            mode="date"
            maxDate={new Date()}
            format="YYYY-MM-DD"
            confirmBtnText={strings('confirm')}
            cancelBtnText={strings('cancel')}
            onDateChange={(date) => {this.setState({dob: date})}}
            />
          </View>
          <View style={[container.row, {marginBottom: 5}]}>
            <View style={[container.row, {flex: 1, paddingRight: 10}]}>
              <Text style={[text.text]}>{strings('email')}</Text>
            </View>
            <TextInput
            value={this.state.email}
            onChangeText={text=>this.setState({email:text})}
            style={[textInput.normal, {backgroundColor: getColor('GREY_LIGHTEN_4'), flex: 3, textAlign: "center"}]}
            keyboardType="default"
            ref={ref=>this.email=ref}
            returnKeyType="next"
            placeholderTextColor={getColor('GREY')}
            />
          </View>
          <View style={[container.row, {marginBottom: 5}]}>
            <View style={[container.row, {flex: 1, paddingRight: 10}]}>
              <Text style={[text.text]}>{strings('gender')}</Text>
            </View>
            <View style={[container.row, {flex: 3}]}>
              <TouchableOpacity style={{flex: 1, borderWidth: 0.5, borderColor: this.state.gender==0?getColor('BLUE'):getColor('white')}} onPress={()=>this.setState({gender: 0})}>
                <Icon name="male" type="foundation" color={this.state.gender==0?getColor('BLUE'):getColor('GREY')} />
              </TouchableOpacity>
              <TouchableOpacity style={{flex: 1, borderWidth: 0.5, borderColor: this.state.gender==1?getColor('PINK'):getColor('white')}} onPress={()=>this.setState({gender: 1})}>
                <Icon name="female" type="foundation" color={this.state.gender==1?getColor('PINK'):getColor('GREY')}/>
              </TouchableOpacity>
            </View>
          </View>
        </View>);
      }
      if(this.state.screen==1){
        return (<View style={[container.column, container.full]}>
          <Text style={[text.title, {marginBottom: 5}]}>{strings('what_activity_are_you_into_these_days')}</Text>
          <Text style={[text.text, {marginBottom: 15}]}>{strings('activity_text_1')}</Text>
          <FlatList
          data={this.props.App.Activity.activities}
          renderItem={this.renderItem}
          numColumns={2}
          keyExtractor={item => item.id.toString()}
          />
        </View>)
      }
      if(this.state.screen==2){
        return (<View style={[container.column, container.full]}>
          <Text style={[text.title, {marginBottom: 5}]}>{strings('your_settings')}</Text>
          <Text style={[text.text, {marginBottom: '15%'}]}>{strings('settings_text_1')}</Text>
          <View style={[container.column, container.card, {backgroundColor: getColor("GREY_LIGHTEN_4"), borderRadius: 10, backgroundColor: getColor('white')}]}>
            <View style={[container.row, {padding: 15, borderBottomWidth: 0.5, borderColor: getColor('GREY')}]}>
              <Text style={[text.text, {flex: 1}]}>{strings("location")}</Text>
              <Switch value={false} onValueChange={()=>{}} />
            </View>
            <View style={[container.row, {padding: 15, borderBottomWidth: 0.5, borderColor: getColor('GREY')}]}>
              <Text style={[text.text, {flex: 1}]}>{strings("distance")}</Text>
              <View style={container.full}>
                <Slider
                value={0}
                minimumValue={0}
                maximumValue={100}
                onValueChange={(value) => {}}
                />
              </View>
            </View>
            <View style={[container.row, {padding: 15}]}>
              <Text style={[text.text, {flex: 1}]}>{strings("age_range")}</Text>
              <View style={container.full}>
                <Slider
                value={0}
                minimumValue={0}
                maximumValue={100}
                onValueChange={(value) => {}}
                />
              </View>
            </View>
          </View>
        </View>)
      }
    }

    barPressed(screen){
      if(this.state.screen>screen){
        this.setState({screen: screen});
        return true;
      }
      return false;
    }

    render() {
      return (<View style={[container.full, {backgroundColor: getColor('white')}]}>
        <ScrollView style={[container.full, {marginTop: Header.HEIGHT}]} contentContainerStyle={[container.column, {justifyContent: "space-between", paddingHorizontal: '15%'}]} keyboardShouldPersistTaps="always">
          <View style={[container.row]}>
            <TouchableOpacity onPress={()=>this.barPressed(0)} style={{flex: 1}}>
              <View style={[line.line, {borderColor: this.state.screen==0?getColor('ELEGANT_COLOR'):getColor('GREY_LIGHTEN_2'), borderWidth:3}]} />
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.barPressed(1)} style={{flex: 1}}>
              <View style={[line.line, {borderColor: this.state.screen==1?getColor('ELEGANT_COLOR'):getColor('GREY_LIGHTEN_2'), borderWidth:3}]} />
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.barPressed(2)} style={{flex: 1}}>
              <View style={[line.line, {borderColor: this.state.screen==2?getColor('ELEGANT_COLOR'):getColor('GREY_LIGHTEN_2'), borderWidth:3}]} />
            </TouchableOpacity>
          </View>
          {this.renderContent()}
        </ScrollView>
        <RibbonButton style={{marginHorizontal: '15%'}} name={strings("continue")} onPress={this.continue} />
        <Loading visible={this.state.loading} />
      </View>)
    }
}

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setIdentity: data=>dispatch(setIdentity(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Setup)