import React, { Component } from 'react';
import { View, Text, Image, StyleSheet} from "react-native";
import {container} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import HeaderColor from './components/HeaderColor';

class Template extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount(){
  }


  render() {
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_4')}]}>

    </View>)
  }
}

const styles = StyleSheet.create({

})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Template)