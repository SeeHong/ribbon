import React, { Component } from 'react';
import {View} from "react-native";
import {container} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {strings} from '../translation/i18n';
import HeaderColor from './components/HeaderColor';
import { WebView } from 'react-native-webview';
import Loading from './components/Loading';
import {TERMS_AND_CONDITIONS_URL_EN, TERMS_AND_CONDITIONS_URL_BN} from '../config';

class TermsConditions extends Component {

  constructor(props) {
    super(props);
    this.state={
        url: props.App.Auth.locale=="en"?TERMS_AND_CONDITIONS_URL_EN:TERMS_AND_CONDITIONS_URL_BN,
        progress: true
    }
  }

  render() {
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_5')}]}>
      <HeaderColor renderBackButton={true} title={strings('terms_and_conditions')} backButtonAction={()=>this.props.navigation.navigate('Home')} />
        <WebView 
        originWhitelist={['*']}
        javaScriptEnabledAndroid={true}
        onLoadProgress={({ nativeEvent }) => {
        if(nativeEvent.progress==1){
            this.setState({progress: false})
        }
        }}
        style={{flex: 1}}
        source={{ uri: this.state.url }} />
        <Loading visible={this.state.progress} />
    </View>)
  }
}

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TermsConditions)