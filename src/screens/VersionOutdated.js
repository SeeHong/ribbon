import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TouchableOpacity, StyleSheet, Linking, Platform } from "react-native";
import {container, button, text, textInput, absolute} from '../styles';
import { connect } from 'react-redux';
import { getColor } from '../colors';
import {Icon, Avatar} from 'react-native-elements';
import {strings} from '../translation/i18n';
import {APPLE_STORE_URL, GOOGLE_STORE_URL} from '../config';

class VersionOutdated extends Component {

  constructor(props) {
    super(props);
    this.updateApp=this.updateApp.bind(this);
  }

  updateApp(){
    let url = "";
    if(Platform.OS=='android'){
        url=GOOGLE_STORE_URL;
    }else{
        url=APPLE_STORE_URL;
    }
    Linking.canOpenURL(url).then(supported => {
        if (supported) {
          Linking.openURL(url);
        }
    });
  }
  
  render() {
    return (<View style={[container.full, container.column, {backgroundColor: getColor('GREY_LIGHTEN_5')}]}>
        <View style={[{height: Dimensions.get('window').height * 2/5, width: Dimensions.get('window').width}, container.center]}>
            <Image source={require('../assets/logo_main.png')} resizeMode="contain" style={{height: Dimensions.get('window').width*2/5, width: Dimensions.get('window').width*2/5}}/>
        </View>
        <Text style={[text.title, {textAlign: "center", marginBottom: 15}]}>{strings('version_outdated_1')}</Text>
        <Text style={[text.text, {textAlign: "center", marginBottom: '20%', marginHorizontal: '5%'}]}>{strings('version_outdated_2')}</Text>
        <TouchableOpacity style={[button.primary, {marginHorizontal: '5%'}]} onPress={this.updateApp}>
            <Text style={button.primary_text}>{strings('update')}</Text>
        </TouchableOpacity>
    </View>)
  }
}

const styles = StyleSheet.create({
})

const mapStateToProps = state => {
  return {
    App: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VersionOutdated)