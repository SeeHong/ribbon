import React, {PureComponent} from 'react';
import { Text, StyleSheet, View } from 'react-native';
import { getColor } from '../../colors';
import {text, container} from '../../styles';
import {Icon} from 'react-native-elements';

export default class BadgeGenderAge extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (<View style={[container.center, styles.badge, {backgroundColor: this.props.gender=='f'?getColor('PINK_LIGHTEN_2'):getColor('INDIGO'), ...this.props.style}]}>
            {this.props.gender=='f'?
            <Icon name="gender-female" type="material-community" color={getColor('white')} size={11}/>:
            <Icon name="gender-male" type="material-community" color={getColor('white')} size={11}/>}
            <Text style={[text.white, {fontSize: 11}]}>{this.props.age}</Text>
        </View>)
    }
}

const styles = StyleSheet.create({
    badge:{borderRadius: 15, flexDirection: "row", height: 25, paddingHorizontal: 8}
})