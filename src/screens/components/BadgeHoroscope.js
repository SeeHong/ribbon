import React, {PureComponent} from 'react';
import { Text, StyleSheet, View } from 'react-native';
import { getColor } from '../../colors';
import {text, container} from '../../styles';

export default class BadgeHoroscope extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (<View style={[container.center, styles.badge, {...this.props.style}]}>
        <Text style={[text.white, {fontSize: 11}]}>{this.props.horoscope}</Text>
      </View>)
    }
}

const styles = StyleSheet.create({
    badge:{borderRadius: 15, flexDirection: "row", height: 25, backgroundColor: getColor('PURPLE'), paddingHorizontal: 8}
})