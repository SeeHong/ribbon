import React, {PureComponent} from 'react';
import { Text, StyleSheet, TouchableOpacity, Image, Dimensions, View } from 'react-native';
import { getColor } from '../../colors';
import { button } from '../../styles';
import LinearGradient from 'react-native-linear-gradient';
import {Avatar} from 'react-native-elements';
import {text, container} from '../../styles';

export default class ChatRoom extends PureComponent {
    constructor(props) {
        super(props);
        this.onPress=this.onPress.bind(this);
    }

    componentDidMount(){
    }

    onPress(){
        this.props.onPress(this.props.ribbon);
    }

    render() {
        return (<TouchableOpacity style={[container.row, {marginBottom: 10, ...this.props.style, borderBottomWidth: 0.5, borderBottomColor: getColor('GREY'), padding: 10}]} onPress={this.onPress}>
            <View style={[{flex: 2}]}>
                <Avatar rounded source={{uri: this.props.chat.img}} size="large"/>
            </View>
            <View style={[container.column, {flex: 5}]}>
                <Text numberOfLines={1} style={[text.text, {marginBottom: 5}]}>{this.props.chat.name}</Text>
                <Text numberOfLines={1} style={text.text}>{this.props.chat.message}</Text>
            </View>
        </TouchableOpacity>)
    }
}

const styles = StyleSheet.create({
})