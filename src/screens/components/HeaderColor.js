import React from 'react';
import { Text, TouchableOpacity, TextInput, View, UIManager } from 'react-native';
import {Icon} from 'react-native-elements';
import {Header} from 'react-navigation-stack';
import LinearGradient from 'react-native-linear-gradient';
import {getColor} from '../../colors';
import {textInput} from '../../styles';

export default class HeaderColor extends React.Component {
  constructor(props){
      super(props);
      this.state= {
        search: "",
        show_search: false
      }
  }

  renderBackButton(){
      if(this.props.renderBackButton){
        return (<TouchableOpacity 
        style={{position:"absolute", top: 0, left: 0, bottom: 0, width: 50, alignItems:"center", justifyContent:"center", zIndex: 1}}
        onPress={()=>this.props.backButtonAction?this.props.backButtonAction():this.props.navigation.goBack()}>
            <Icon
            name='chevron-left'
            type='feather'
            color={getColor('ELEGANT_COLOR')}
            size={30}
            />
        </TouchableOpacity>)
      }
  }

  renderDrawer(){
    if(this.props.drawal){
        return (<TouchableOpacity style={{position:"absolute", top: 0, left: 0, bottom: 0, width: 50, alignItems:"center", justifyContent:"center", zIndex: 1}}
        onPress={()=>this.props.openDrawal()}>
            <Icon name="menu" type="feather" color={getColor('white')} size={30} />
        </TouchableOpacity>)
    }
    return null;
  }

  renderRightFunction(){
      if(this.props.renderRightAction){
        return (<TouchableOpacity 
            style={{position:"absolute", top: 0, right: 0, bottom: 0, width: 50, alignItems:"center", justifyContent:"center", zIndex: 1,}}
            onPress={()=>this.props.rightButtonAction?this.props.rightButtonAction():null}>
                <Icon
                name={this.props.rightButtonName}
                type={this.props.rightButtonType}
                color={this.props.rightButtonColor?this.props.rightButtonColor:getColor('ELEGANT_COLOR')}
                />
            </TouchableOpacity>)
      }
      return null;
  }

  renderSearch(){
    return (<View style={{height: '100%', width: '100%',justifyContent:"center"}}>
    <TouchableOpacity
    style={{position:"absolute", top: 0, left: 0, bottom: 0, width: 50, alignItems:"center", justifyContent:"center", zIndex: 1}}
    onPress={()=>this.props.renderSearchButton(this.state.search)}>
      <Icon
      name='search'
      type='feather'
      color={getColor('APP_1')}
      size={18}
      />
    </TouchableOpacity>
    <TextInput
    placeholder='Search'
    onChangeText={text => {this.setState({search: text})}}
    value={this.state.search}
    style={[textInput.normal, {marginHorizontal: 50, borderRadius: 5, marginVertical: '2%'}]}
    returnKeyLabel="done"
    ref={ref=>this.headerSearch = ref}
    returnKeyType="done"
    onSubmitEditing={()=>{this.props.renderSearchButton(this.state.search)}}
    />
    <TouchableOpacity
    style={{position:"absolute", top: 0, right: 0, bottom: 0, width: 50, alignItems:"center", justifyContent:"center", zIndex: 1,}}
    onPress={()=>{this.setState({search: "", show_search: false}, ()=>{this.props.onSearchCancel?this.props.onSearchCancel():null})}}>
      <Icon
      name='x'
      type='feather'
      color={getColor('ELEGANT_COLOR')}
      size={18}
      />
    </TouchableOpacity>
  </View>)
  }

  render() {
    return (<View style={{height: Header.HEIGHT, justifyContent:"center" , alignItems: "flex-start", backgroundColor: getColor('white')}}>
        {this.renderBackButton()}
        {this.renderDrawer()}
        {this.renderRightFunction()}
        <Text style={{color: getColor('ELEGANT_COLOR'), fontSize: 20, marginLeft: this.props.renderBackButton || this.props.drawal ? 50 : 15, marginRight: 100, fontWeight: 'bold'}} numberOfLines={1}>{this.props.title}</Text>
    </View>)
  }
}