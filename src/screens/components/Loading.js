import React, {PureComponent} from 'react';
import { View, ActivityIndicator} from 'react-native';
import Modal from "react-native-modal";
import { getColor } from '../../colors';
import {container} from '../../styles';
import {setLanguage} from '../../translation/i18n';

export default class Loading extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (
        <Modal isVisible={this.props.visible}
        style={{flex: 1, padding: 0, margin: 0, alignItems: "center", justifyContent: "center"}}
        animationIn="fadeIn"
        animationInTiming={300}
        animationOut="fadeOut"
        animationOutTiming={300}
        hasBackdrop={true}>
            <View style={{padding: 20, backgroundColor: getColor('white'), borderRadius: 10, shadowOffset:{  width: 2,  height: 2,  },
                shadowColor: getColor('black'), shadowOpacity: 0.8, elevation: 1}}>
                <ActivityIndicator size="large" color={getColor("APP_1")} />
            </View>
        </Modal>
        )
    }
}