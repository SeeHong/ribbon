import React, {PureComponent} from 'react';
import { TextInput, View, Text, TouchableOpacity, Keyboard} from 'react-native';
import {container, otp} from '../../styles';
import {validateNumber} from '../../validation';

export default class OTPComponent extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            otp: ""
        }
        this.focus_otp=this.focus_otp.bind(this);
        this.onChangeText=this.onChangeText.bind(this);
    }

    focus_otp(){
        this.clearOtp();
        this.otp_input.blur();
        this.otp_input.focus();
    }

    submitOtp(){
        Keyboard.dismiss();
        this.props.otp?this.props.otp(this.state.otp):null;
        this.clearOtp();
    }

    clearOtp(){
        this.setState({otp: ""});
    }

    onChangeText(text){
        if(!validateNumber(text)){
            return false;
        }
        if(text.length==7){
            return false;
        }
        this.setState({otp: text}, ()=>{
            if(text.length==6){
                this.submitOtp();
            }
        });
    }

    checkText(val){
        return this.state.otp.charAt(val);
    }

    render() {
        return (<View style={[container.row, {alignItems: "center", justifyContent: 'center', marginVertical: 10}]}>
            <TouchableOpacity style={otp.view} onPress={this.focus_otp}>
                <Text style={otp.text}>{this.checkText(0)}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={otp.view} onPress={this.focus_otp}>
                <Text style={otp.text}>{this.checkText(1)}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={otp.view} onPress={this.focus_otp}>
                <Text style={otp.text}>{this.checkText(2)}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={otp.view} onPress={this.focus_otp}>
                <Text style={otp.text}>{this.checkText(3)}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={otp.view} onPress={this.focus_otp}>
                <Text style={otp.text}>{this.checkText(4)}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={otp.view} onPress={this.focus_otp}>
                <Text style={otp.text}>{this.checkText(5)}</Text>
            </TouchableOpacity>
            <TextInput
            style={{position: 'absolute', left: 10000}}
            keyboardType="phone-pad"
            onChangeText={this.onChangeText}
            value={this.state.otp}
            returnKeyType="done"
            ref={ref=>this.otp_input=ref}
            />
        </View>)
    }
}