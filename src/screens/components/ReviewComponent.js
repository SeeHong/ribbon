import React, {PureComponent} from 'react';
import { Text, StyleSheet, TouchableOpacity, Image, Dimensions, View } from 'react-native';
import { getColor } from '../../colors';
import { button } from '../../styles';
import LinearGradient from 'react-native-linear-gradient';
import {Avatar} from 'react-native-elements';
import {text, container} from '../../styles';

export default class ReviewComponent extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (<View style={[container.row, {marginBottom: 10, ...this.props.style, borderBottomWidth: 0.5, borderBottomColor: getColor('GREY'), padding: 10}]}>
            <View style={[{flex: 2}]}>
                <Avatar rounded source={{uri: this.props.review.avatar}} size="large"/>
            </View>
            <View style={[container.column, {flex: 5}]}>
                <Text numberOfLines={1} style={[text.title, {marginBottom: 5}]}>{this.props.review.name}</Text>
                <Text style={[text.text, {marginBottom: 5}]}>{this.props.review.text}</Text>
                <Text style={text.muted}>{this.props.review.created_at}</Text>
            </View>
        </View>)
    }
}

const styles = StyleSheet.create({
})