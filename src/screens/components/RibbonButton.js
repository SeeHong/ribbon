import React, {PureComponent} from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';
import { getColor } from '../../colors';
import { button } from '../../styles';
import LinearGradient from 'react-native-linear-gradient';

export default class RibbonButton extends PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (<TouchableOpacity style={[{marginBottom: 10, ...this.props.style} ]} onPress={this.props.onPress}>
            <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={[getColor('APP_1'), getColor('APP_2')]} style={styles.getStarted}>
                <Text style={[button.primary_text]}>{this.props.name}</Text>
            </LinearGradient>
        </TouchableOpacity>)
    }
}

const styles = StyleSheet.create({
    getStarted: {height: 45, alignItems:"center", justifyContent:'center', width: "100%", borderRadius: 15, paddingHorizontal: 15}
})