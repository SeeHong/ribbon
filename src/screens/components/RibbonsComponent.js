import React, {PureComponent} from 'react';
import { Text, StyleSheet, TouchableOpacity, Image, Dimensions, View } from 'react-native';
import { getColor } from '../../colors';
import { button } from '../../styles';
import LinearGradient from 'react-native-linear-gradient';
import {Icon} from 'react-native-elements';
import {text, absolute,container} from '../../styles';
import BadgeGenderAge from './BadgeGenderAge';
import BadgeHoroscope from './BadgeHoroscope';

export default class RibbonComponent extends PureComponent {
    constructor(props) {
        super(props);
        this.onPress=this.onPress.bind(this);
    }

    onPress(){
        this.props.onPress(this.props.ribbon);
    }

    render() {
        return (<TouchableOpacity style={[{marginBottom: 10, ...this.props.style, marginRight: this.props.index%2==0?10:0, borderTopRightRadius: 10, borderTopLeftRadius: 10, width: ((Dimensions.get('window').width*this.props.usableWidth)/2)-5}]} onPress={this.onPress}>
            <View style={{height: (Dimensions.get('window').width*this.props.usableWidth)/2, width: ((Dimensions.get('window').width*this.props.usableWidth)/2)-5}}>
                <Image source={{uri: this.props.ribbon.img}} style={{height: (Dimensions.get('window').width*this.props.usableWidth)/2, width: ((Dimensions.get('window').width*this.props.usableWidth)/2)-5, borderTopRightRadius: 10, borderTopLeftRadius: 10}}/>
                <View style={[container.row, absolute.bottom, {bottom: 5, backgroundColor: 'rgba(255,255,255,0.7)', left: 0, right: 0, paddingVertical: 5}]}>
                    <Text numberOfLines={1} style={[text.text, {flex: 3, fontSize: 12}]}>{this.props.ribbon.name}</Text>
                    <BadgeGenderAge age={this.props.ribbon.age} gender={this.props.ribbon.gender}/>
                    <BadgeHoroscope horoscope={this.props.ribbon.horoscope}/>
                </View>
            </View>
            <Text style={[text.text, {marginBottom: 5}]}>{this.props.ribbon.job}, {this.props.ribbon.distance}km</Text>
            <Text numberOfLines={2} style={[text.text, {color: getColor('GREY')}]}>{this.props.ribbon.description}</Text>
        </TouchableOpacity>)
    }
}

const styles = StyleSheet.create({
    getStarted: {height: 45, alignItems:"center", justifyContent:'center', width: "100%", borderRadius: 15}
})