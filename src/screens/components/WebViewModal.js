import React, { Component } from 'react';
import {View, Text, ActivityIndicator, Easing, TouchableOpacity, SafeAreaView} from 'react-native';
import { WebView } from 'react-native-webview';
import { strings } from '../../translation/i18n';
import {getColor} from '../../colors';
import LinearGradient from 'react-native-linear-gradient';
import {Icon} from 'react-native-elements';
import Modal from "react-native-modal";
import {Header} from 'react-navigation-stack';

export default class WebViewModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      progress: true,
    }
  }

  closeModal(){
    this.state.progress=true,
    this.props.closeModal();
  }

  renderIndicator(){
    if(this.state.progress){
      return(<View style={{position:"absolute", top: 0, right: 0, bottom: 0, padding:10, alignItems: "center", justifyContent: "center"}}>
        <ActivityIndicator size="small" color="#fff" />
      </View>)
    }
    return null;
  }

  handleMessage(message) {
    console.log(message.nativeEvent.data);
  }

  render() {
    return (
    <Modal isVisible={this.props.visible}
    animationIn="slideInUp"
    animationOut="slideOutDown"
    onRequestClose={() => { this.closeModal(); } }
    style = {{  margin: 0 }}
    hasBackdrop={true}>
      <View style={{flex: 1, backgroundColor: getColor('white')}}>
        <SafeAreaView style={{flex: 1, flexDirection: "column"}}>
          <LinearGradient colors={[getColor('APP_1'), getColor('APP_2')]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={{height: Header.HEIGHT, alignItems: 'center', justifyContent: "center", backgroundColor: "#fff"}}>
            <TouchableOpacity 
            onPress={()=>this.closeModal()}
            style={{position:"absolute", top: 0, left: 0, bottom: 0, padding:10, alignItems: "center", justifyContent: "center"}}>
              <Icon
              name="chevron-left"
              type="feather"
              color="#fff"
              />
            </TouchableOpacity>
            <Text numberOfLines={1} style={{fontSize:18, color: '#fff'}}>{this.props.title}</Text>
            {this.renderIndicator()}
          </LinearGradient>
          <WebView 
          originWhitelist={['*']}
          javaScriptEnabledAndroid={true}
          onLoadProgress={({ nativeEvent }) => {
            if(nativeEvent.progress==1){
              this.setState({progress: false})
            }
          }}
          style={{flex: 1}}
          source={{ uri: this.props.url }} />
        </SafeAreaView>
      </View>
    </Modal>
    );
  }
}