import { getColor } from "./colors";
import {Platform} from 'react-native';

export const container = {
    center: {alignItems:"center", justifyContent:'center'},
    full: {flex:1},
    row: {flexDirection: 'row', alignItems: 'center'},
    column: {flexDirection: "column"},
    border: {borderWidth: 1, borderColor: getColor('ELEGANT_COLOR'), borderRadius: 0},
    card: {shadowColor: getColor('black'), shadowOffset: { width: 1, height: 2}, shadowOpacity: 0.3, shadowRadius: 5, elevation: 1},
    overlay: {backgroundColor: "rgba(0,0,0,0.5)"}
}

export const line={
    line: {borderBottomWidth: 1, borderColor: getColor('GREY'), width: '100%', marginVertical: 10}
}

export const button = {
    primary: {alignItems:"center", justifyContent:'center', backgroundColor: getColor('APP_1'), height: 45},
    danger: {alignItems:"center", justifyContent:'center', backgroundColor: getColor('DANGER_COLOR'), height: 45},
    success: {alignItems:"center", justifyContent:'center', backgroundColor: getColor('SUCCESS_COLOR'), height: 45},
    info: {alignItems:"center", justifyContent:'center', backgroundColor: getColor('INFO_COLOR'), height: 45},
    facebook: {alignItems:"center", justifyContent:'center', flexDirection:"row", backgroundColor: getColor('INDIGO'), height: 45, borderRadius: 15},
    google: {alignItems:"center", justifyContent:'center', flexDirection:"row", backgroundColor: getColor('RED'), height: 45, borderRadius: 15},
    info_outline: {alignItems:"center", justifyContent:'center', borderColor: getColor('INFO_COLOR'), borderWidth: 1, height: 45},
    white_outline_round: {alignItems:"center", justifyContent:'center', borderColor: getColor('white'), borderWidth: 2, height: 45, borderRadius: 15},
    disabled: {alignItems:"center", justifyContent:'center', backgroundColor: getColor('GREY'), height: 45},
    primary_text: {color: getColor('white'), fontSize: 16},
    info_outline_text: {color: getColor('INFO_COLOR'), fontSize: 14},
    danger_text: {color: getColor('white'), fontSize: 16},
    disabled_text: {color: getColor('white'), fontSize: 16},
    primary_invert: {alignItems:"center", justifyContent:'center', borderColor: getColor('APP_1'), height: 45, borderRadius: 25, borderWidth: 1, backgroundColor: 'transparent'},
    primary_text_invert: {color: getColor('APP_1'), fontSize: 16},
}

export const text = {
    muted: {fontSize: Platform.OS=="android"?11:12, color: getColor('GREY_DARKEN_3')},
    white: {fontSize: Platform.OS=="android"?13:14, color: getColor('white')},
    link: {fontSize: Platform.OS=="android"?13:14, color: getColor('PRIMARY_COLOR')},
    text: {fontSize: Platform.OS=="android"?13:14, color: getColor('ELEGANT_COLOR')},
    disabled: {fontSize: Platform.OS=="android"?13:14, color: getColor('GREY')},
    small: {fontSize: Platform.OS=="android"?13:14, color: getColor('APP_2')},
    title: {fontSize: Platform.OS=="android"?17:18, color: getColor("ELEGANT_COLOR"), fontWeight: "bold"},
    placeHolder: {fontSize: Platform.OS=="android"?15:16, color: getColor('APP_2')},
    login_title_1: {fontSize: Platform.OS=="android"?38:35, color: getColor("white"), textAlign: "center"},
    login_title_2: {fontSize: Platform.OS=="android"?20:18, color: getColor("white"), textAlign: "center"},
    register_title_1: {fontSize: Platform.OS=="android"?22:20, color: getColor("ELEGANT_COLOR"), textAlign: "center", marginBottom:10},
    register_title_2: {fontSize: Platform.OS=="android"?16:15, color: getColor("GREY_DARKEN_2"), textAlign: "center", marginBottom: 15},
}

export const textInput = {
    normal : {fontSize: 14, color: getColor('ELEGANT_COLOR'), height:35, paddingHorizontal: 10, margin: 0, backgroundColor: getColor('GREY_LIGHTEN_5'), borderRadius: 5},
    disabled : {flex: 1, fontSize: 16, color: getColor('GREY_LIGHTEN_1'), height: 30, padding: 0, margin: 0},
    theme: { colors: { primary: getColor("APP_1"), background: getColor('white'), text: getColor('ELEGANT_COLOR') } },
    paper: {marginBottom: 5, padding: 0},
    border: {borderWidth: 1, borderColor: getColor('GREY_DARKEN_2'), borderRadius: 5, paddingVertical: 10}
}

export const absolute ={
    bottom: {position:'absolute', bottom: 0},
    top: {position:'absolute', top: 0},
    absolute: {position: "absolute"}
}

export const otp={
    view: {borderBottomWidth: 2, borderColor: getColor('APP_1'), marginHorizontal: 5, height: 30, width: 30, alignItems: 'center', justifyContent: "center"},
    text: {color: getColor('ELEGANT_COLOR'), fontSize: 16}
}

export const pin={
    view: {borderRadius: 10, borderWidth: 2, borderColor: getColor('APP_1'), marginHorizontal: 5, height: 50, width: 50, alignItems: 'center', justifyContent: "center"},
    text: {color: getColor('white'), fontSize: 16}
}

export const overlay={
    black: {backgroundColor: "rgba(0,0,0,0.7)"}
}