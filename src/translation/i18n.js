import i18n from 'i18n-js';
import en from './en.json';
import Store from '../redux/store';
import {setLocale} from '../redux/actions/AuthActions';

i18n.fallbacks = true;

i18n.translations = {
  en
};

export function setLanguage(lang) {
    Store.store.dispatch(setLocale(lang));
    i18n.locale = lang; 
}

export function getLanguage(){
    return i18n.locale;
}

export function strings(name, params) {
   return i18n.t(name);
};

export default i18n;
