export const validateUsername = (txt) => {
    if(txt.length >= 11 && txt.length <= 15) {
        var regex = /^\d+$/;
        return regex.test(txt);
    }
    return false;
}

export const validateNumber = (number) => {
    if(number==""){
        return true;
    }
    var regex = /^\d+$/;
    return regex.test(number);
}

export const validateCardNumber = (number) => {
    var regex = /^\d{16}$/;
    return regex.test(number);
}

export const validatePassword = (password) => {
    if(password.length > 7 && password.length < 20) {
        return true;
    }
    return false;
}

export const validateConfirmPassword = (first, txt) => {

    if(first === txt) {
        return true;
    }
    return false;
}

export const validateName = (name) => {
    var regex = /^['a-zA-Z\u4e00-\u9eff ]*$/i;
    return regex.test(name);
}

export const validateAlphaNumName = (name) => {
    var regex = /^[a-zA-Z0-9]*$/;
    return regex.test(name);
}

export const validateEmail = (txt) => {
    var regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(txt);
}

export const validateSearchKey = (txt) => {
    if(txt.length > 0){
        var regex = /^[a-zA-Z'\-\s]+$/;
        return regex.test(txt);
    }
    return false;
}

export const validateRemarks = (txt) => {
    var regex = /^[-'a-zA-Z0-9\u4e00-\u9eff.,: ]{1,20}$/i;
    return regex.test(txt);
}

export const validateWhiteSpaces = (attribute) => {
    if (attribute.slice(0, 1) == ' ') {
        return attribute.replace(/\s+$/, '');
    }
    else {
        return attribute.replace(/\s+$/, ' ');
    }
}

export const validateRequired = (attribute) => {
    if (attribute=="" || attribute.trim() == '' || attribute == null) {
        return false;
    }
    return true;
}

export const validatePinLength = (pin) => {
    if (pin.length == 4) {
        return true;
    }

    return false;
}

export const validateAmount = (amount) => {
    // var regex = /^(\d*\.)?\d{0,2}$/;
    if(amount==""){
        return false;
    }
    if(amount.length>11){
        return false;
    }
    var regex = /^(?!00)(?!01)(?!02)(?!03)(?!04)(?!05)(?!06)(?!07)(?!08)(?!09)\d+\.?(\d{1,2})?$/;
    return regex.test(amount);
}

export const validateAmountZero = (amount) => {
    if (amount == 0) {
        return true;
    }

    return false;
}

export const validateTransactionPin = (tpin) => {
    if (tpin.length < 6) {
        return true;
    }

    return false;
}